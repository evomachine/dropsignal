# -*- mode: python -*-

# This is a specification file for PyInstaller (http://www.pyinstaller.org/)

# To build the executable, install dropsignal with pip,
# > pip install https://gitlab.com/evomachine/dropsignal/repository/archive.zip?ref=master
# download this file and then:
# > pyinstaller windows_executable.spec
# You can now distribute the dist/dropsignal folder.

block_cipher = None
from PyInstaller.utils.hooks import collect_data_files, eval_statement
dropsignal_files = collect_data_files( 'dropsignal', subdir=None, include_py_files=False )
import dropsignal.plugins
import dropsignal
import tempfile
import os
import pkg_resources


with tempfile.TemporaryDirectory() as dir:
        dropsignal_files = list(dropsignal_files) + [(os.path.join(dir,'VERSION'),'dropsignal'), (os.path.join(dir,'INSTALLED_PLUGINS'),'dropsignal')]
        print('Collected {} files'.format(len(dropsignal_files)))
        for f in dropsignal_files:
            print(f)

        with open(os.path.join(dir,'VERSION'),'w') as f:
                dists = [d for d in pkg_resources.working_set if d.key=='dropsignal']
                f.write(dists[0].version)
                print('Dropsignal version {}'.format(dists[0].version))
        dropsignal_exe = os.path.join(os.path.dirname(dropsignal.__file__),'webgui.py')
        icon = os.path.join(os.path.dirname(dropsignal.__file__),'static','dropsignal.ico')
        plugins = ['dropsignal.plugins.'+x for x in dropsignal.plugins.INSTALLED]

        with open(os.path.join(dir,'INSTALLED_PLUGINS'),'w') as f:
                f.write(",".join(dropsignal.plugins.INSTALLED))

        print('Plugins')
        print(dropsignal.plugins.INSTALLED)
        a = Analysis([dropsignal_exe],
                                 pathex=['C:\\'],
                                 binaries=[],
                                 datas=dropsignal_files,
                                 hiddenimports=plugins,
                                 hookspath=[],
                                 runtime_hooks=[],
                                 excludes=[],
                                 win_no_prefer_redirects=False,
                                 win_private_assemblies=False,
                                 cipher=block_cipher)
        pyz = PYZ(a.pure, a.zipped_data,
                                 cipher=block_cipher)
        exe = EXE(pyz,
                          a.scripts,
                          exclude_binaries=True,
                          name='dropsignal',
                          debug=False,
                          strip=False,
                          upx=True,
                          console=True,
                          icon=icon,
                          )
        coll = COLLECT(exe,
                                   a.binaries,
                                   a.zipfiles,
                                   a.datas,
                                   strip=False,
                                   upx=True,
                                   name='dropsignal')


import numpy as np
import pytest
import dropsignal.processing.signal as signal


def test_detection_wo_filter():
    sig = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
                    1, 1, 1, 0, 0, 0, 0, 0, 0, 0])
    center, inter, detect, s, e = signal.detect_peaks(sig, 'boxcar', 0, threshold=0.9)
    print(center)
    print(inter)
    print(detect)
    print(s)
    print(e)
    assert all(s == [11, 26])
    assert all(e == [20, 33])
    assert all(center == [15, 29])
    assert all(inter == [2, 23, 39])
    assert all(detect == sig)


def test_detection_border():
    sig = np.array([1, 0, 0, 0, 1, 1, 1, 0, 0, 1])
    center, inter, detect, s, e = signal.detect_peaks(sig, 'boxcar', 0, threshold=0.9)
    print(center)
    print(inter)
    print(detect)
    print(s)
    print(e)
    assert all(s == [4])
    assert all(e == [7])
    assert all(center == [5])
    assert all(inter == [1, 9])
    assert all(detect == sig)

""" Test Quality check functions """
from functools import partial
import logging
import pandas
import dropsignal.processing.quality as quality

logger = logging.getLogger('dropsignal.quality')
logger.setLevel(logging.DEBUG)


def test_quality_check():
    runs = pandas.DataFrame({
        'id': range(12),
        'duration': [10,  10,  10, 10, 0.1, 10,   10,   9, 11, 9.9, 8.7, 10],
        'peaks':    [100, 10, 100, 99, 100, 100, 101, 100, 99,  99, 99,  80],
    })
    goods = [1,   0,   1,  0,  0,     1,   0,   1,  1, 1, 1, 0]
    qc_tests = [partial(quality.qc_length, threshold=1),
                partial(quality.qc_peak_nb, expected=100),
                partial(quality.qc_coalescence, thres=4)]
    bad_tags = ("partial_train", "too_many_droplets",
                "dramatic_coalescence", "probable_misscount",
                "too_short")
    copy = runs.copy()
    runs_out, bad_tags = quality.check_quality(qc_tests, runs, bad_tags)
    print(runs_out)
    assert all(runs == copy)
    assert all(runs_out.good == goods)
    assert 'partial_train' in runs_out.loc[1, 'tags']
    assert 'too_short' in runs_out.loc[4, 'tags']
    assert 'probable_misscount' in runs_out.loc[3, 'tags']
    assert 'dramatic_coalescence' in runs_out.loc[11, 'tags']
    assert 'too_few_droplets' in runs_out.loc[1, 'tags']
    assert 'coalesced' in runs_out.loc[9, 'tags']
    assert 'too_many_droplets' in runs_out.loc[6, 'tags']

from dropsignal.processing.rebuild import get_coalescent_position_additive_size


def test_get_coalescent_position_additive_size():
    ps = [1]*5
    fixtures = [
        [[2, 1, 1, 1], [0]],
        [[1, 2, 1, 1], [1]],
        [[1, 1, 2, 1], [2]],
        [[1, 1, 1, 2], [3]],
        [[3, 1, 1], [0, 1]],
        [[2, 2, 1], [0, 2]],
        [[2, 1, 2], [0, 3]],
        [[1, 3, 1], [1, 2]],
        [[1, 2, 2], [1, 3]],
        [[1, 1, 3], [2, 3]],
        [[4, 1], [0, 1, 2]],
        [[3, 2], [0, 1, 3]],
        [[2, 3], [0, 2, 3]],
        [[1, 4], [1, 2, 3]],
        [[5], [0, 1, 2, 3]]]
    for ns, sol in fixtures:
        msg = "{}-->{} Correct is {}, proposed is: {}".format(ps, ns, sol,
                                                              get_coalescent_position_additive_size(ns, ps)[0])
        assert get_coalescent_position_additive_size(ns, ps)[0] == sol, msg

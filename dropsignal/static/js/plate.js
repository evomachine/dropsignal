// plate.js - An interactive microwell plate reprensentation.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function Plate(svg, width, height, droplets, well_to_color, buttons){
    // Extract row and columns names.

    var rows = d3.map(droplets, function(d){return d.well[0]}).keys().sort()
    var columns = d3.map(droplets, function(d){return d.well.substring(1)}).keys().map(function(d){return +d})
    columns.sort(function(a, b){return a-b})
    rows.sort()

    // Setup svg
    this.svg = svg
    this.svg.attr('width', width).attr('height', height)
    var r = Math.min(0.4*height/(rows.length+1), 0.4*width/columns.length)

    // Set scales
    this.scale_col = d3.scaleBand().range([0,width]).domain(columns).padding(r)
    this.scale_row = d3.scaleBand().range([0,height]).domain(rows).padding(r)

    this.mode = 'display'

    if (buttons == true){
	var ddiv = d3.select(svg.node().parentNode.insertBefore(document.createElement('form'), svg.node()))
	var div = ddiv.append('span').attr('data-placement','bottom')
	    .attr('data-toggle','tooltip')
	    .style('float','right')
	    .style('margin-right','30px')
	    .style('display','inline-block')
	    .attr('title','Choose click action').append('div')
	    .attr('class','btn-group btn-group-toggle')
	    .attr('data-toggle','buttons')
	    .style('display','inline-block')
	div.append('label').text('Display')
	    .on('click',() => {this.mode = 'display'; this.update()})
	    .attr('class', 'btn btn-default form-check-label active')
	    .append('input').attr('type','radio').attr('name','mode')
	    .property('checked',this.mode=='display')
	div.append('label').text('Selection')
	    .on('click',() => {this.mode = 'selection'; this.update()})
	    .attr('class', 'btn btn-default form-check-label')
	    .append('input').attr('type','radio').attr('name','mode')
	    .property('checked',this.mode=='selection')
	var div2 = ddiv.append('span')
	    .attr('data-toggle','tooltip')
	    .style('float','left')
	    .style('margin-right','30px')
	    .style('display','inline-block')
	    .attr('title','On the current microplate...').append('div')
	    .attr('data-toggle','buttons')
	    .style('display','inline-block')
	div2.append('button')
	    .text('None')
	    .on('click', () => {this.stub_get_current_selection().clear(); this.stub_update_display()})
	    .attr('class','btn btn-default')
	div2.append('button')
	    .text('All')
	    .on('click', () => {this.stub_get_current_selection().all(); this.stub_update_display()})
	    .attr('class','btn btn-default')
    }

    // Find proportion of selected entries per well.
    var nested_well = d3.nest()
	.key(function(d) { return d.well })
	.rollup(function(values) {
	    return d3.sum(values, function(d) { return d.selected })/values.length
	})
	.entries(droplets)

    this.well_to_color = well_to_color
    this.r = r
    this.width = width
    this.selected_well = undefined
    this.droplets = droplets


    var self = this
    // This might be a hack...
    // These functions have to be overridden with something useful see main_dynamics.js
    this.stub_get_current_selection = function(){return self.get_current_selection(self.mode)}
    this.stub_update_display = function(){return self.update_display()}
    this.click_well = function (d){
	self.selected_well = d.key
	self.stub_get_current_selection().select_well(d.key)
	self.stub_update_display()}
    this.click_row = function (d){self.stub_get_current_selection().select_row(d);self.stub_update_display()}
    this.click_col = function (d){self.stub_get_current_selection().select_column(d);self.stub_update_display()}
    this.click_drop = function (d){self.stub_get_current_selection().select_droplet(d.id_exp);self.stub_update_display()}



    // Add one circle per well.
    this.wells = this.svg.append('g').attr('class', 'well')
    this.drops = this.svg.append('g').attr('class', 'drops')
	.attr('transform', 'translate(0,'+(height-1.2*r)+')')
    this.selected_well_text = this.drops.append('text')
	.attr('text-anchor','end')
	.attr('x',this.width*0.3)

    this.wells.selectAll('circle')
	.data(nested_well)
	.enter().append('circle')
	.style('fill',function(d){return well_to_color[d.key]})
	.style('opacity',function(d){return d.value==0?'.3':d.value==1?'1':0.7})
	.attr('cy', function(d) { return self.scale_row(d.key[0])})
	.attr('cx', function(d) { return self.scale_col(d.key.substring(1))})
	.attr('r', r)
	.on('click',this.click_well)

    // Add row and columns markers
    var tri = d3.symbol().type(d3.symbolTriangle)
    var row_symbols = this.svg.append('g')
	.attr('id','row_symbols')
	.selectAll('g')
	.data(rows).enter().append('g')
	.attr('transform', function(d) { return 'translate(' + 10 + ',' + self.scale_row(d) + ')'})
	.on('click',this.click_row)

    row_symbols.append('path')
	.style('opacity','.10')
	.attr('transform',' rotate(90)')
	.attr('d', tri)
	.style('fill', 'black')
    row_symbols.append('text')
	.style('alignment-baseline','bottom')
	.style('text-anchor','middle')
	.text(function(d){return d})
	.style('opacity','.6')

    var col_symbols = this.svg.append('g')
	.attr('id','col_symbols')
	.selectAll('g')
	.data(columns).enter().append('g')
	.attr('transform', function(d) { return 'translate(' + self.scale_col(d) + ',' +15  + ')'})
	.on('click',this.click_col)
    col_symbols.append('path')
	.attr('d', tri)
	.style('fill', 'black')
	.attr('transform',' rotate(180)')
	.style('opacity','.10')
    col_symbols.append('text')
	.attr('alignment-baseline','bottom')
	.style('text-anchor','middle')
	.text(function(d){return d})
	.style('opacity','.6')
}

Plate.prototype.update = function(selected){
    if (selected == undefined){
	selected = this.stub_get_current_selection().selected
    }
    var nested_well = d3.nest()
	.key(function(d) { return d.well })
	.rollup(function(values) {
	    return d3.sum(values, d => selected[d.id_exp])/values.length
	})
	.entries(this.droplets)
    this.wells.selectAll('circle')
	.data(nested_well)
	.style('stroke',function(d){return d.value==1?'grey':null})
	.style('opacity',function(d){return d.value==0?'.3':d.value==1?'1':0.7})
    if (this.selected_well != undefined){
	this.draw_droplets(this.selected_well, selected)
    }
}

Plate.prototype.draw = function(droplets){
    // Update color of the wells.
    var nested_well = d3.nest()
	.key(function(d) { return d.well })
	.rollup(function(values) {
	    return d3.sum(values, function(d) { return d.selected })/values.length
	})
	.entries(droplets)
    this.wells.selectAll('circle')
	.data(nested_well)
	.style('stroke',function(d){return d.value==1?'grey':null})
	.style('opacity',function(d){return d.value==0?'.3':d.value==1?'1':0.7})
    if (this.selected_well != undefined){
	this.draw_droplets(this.selected_well)
    }
}

Plate.prototype.draw_droplets = function(well, selected){
    var drops_in_well = this.droplets.filter(e => e.well== well)
    var small_r = Math.min(this.r*0.8, this.width/(2*drops_in_well.length))
    var scale_drops = d3.scaleBand()
	    .range([0.3*this.width,this.width])
	    .domain(d3.range(drops_in_well.length))
	    .padding(small_r)
    this.selected_well_text.text(drops_in_well.length+' drops in '+well+':')
    var circle_drops = this.drops.selectAll('g').data(drops_in_well)
    circle_drops.exit().remove()
    var enter = circle_drops.enter()
	    .append('g')
	    .merge(circle_drops)
	    .html('')
	    .attr('transform', function(d,i) {return 'translate('+scale_drops(i)+',0)'})
	    .on('click', this.click_drop)
    enter.append('circle')
	.style('fill', d => this.well_to_color[d.well])
	.style('opacity', d => selected[d.id_exp]?'1':'0.3')
	.attr('r', small_r)
    enter.append('text')
	.text(function(d){return d.droplet_id})
	.style('text-anchor','middle')
}

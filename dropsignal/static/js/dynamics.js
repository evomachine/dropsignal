// dynamics.js - Display droplet dynamics and scatterplot
// This file is part of the dropsignal package.
// Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---


// Dependencies
var getParameterByName, setParameterByName // Defined in uri_variables.js


// Functions.
function timeFormat(num) {
    var h = Math.floor( num / 3600 )
    var m = Math.floor((num - h * 3600) / 60 )
    var s = num - (h * 3600 + m * 60)
    return ( h < 10 ? '0' + h : h ) + ':' + ( m < 10 ? '0' + m : m ) + ':' + ( s < 10 ? '0' + Math.round(s) : Math.round(s) )
}

function Dynamics(svg, ui, protocol, droplets, display_selection){
    // Bind attributes.
    this.svg = svg
    this.ui = ui
    this.droplets = droplets
    this.protocol = protocol

    // Save current request
    this.current_request = ''
    this.last_request = ''
    this.data = undefined

    // Marker sizes options
    this.marker_sizes = {stroke_thick:2.5, stroke_thin:1.5,
			 circle_thin:1.8, circle_thick:3.5,
			 font_size:1}

    // Setup svg.
    this.rect = svg.node().getBoundingClientRect()

    this.svg.append('defs').html(`<filter id="blurFilter">
    <feGaussianBlur in="SourceGraphic" stdDeviation="1.5" />
    </filter>`)
    this.spinner = svg.append('g').attr('opacity',0).attr('id','spinner')
	.html(`
  <rect width="300" height="170" style="fill:white;stroke:black;stroke-width:10px"/>
  <rect width="320" height="70" x="-10" y="65" class="tube" />
  <rect width="5" height="135" x="150" class="laser" />
  <rect width="20" height="20" x="142.5" />
  <circle cx="-40" cy="100px" r="30px" class="droplet"/>
  <circle cx="30" cy="100px" r="30px" class="droplet"/>
  <circle cx="100" cy="100px" r="30px" class="droplet" id="first_spin"/>
  <circle cx="170" cy="100px" r="30px" class="droplet" />
  <circle cx="240" cy="100px" r="30px" class="droplet" />
  <circle cx="310" cy="100px" r="30px" class="droplet" />
  <rect width="450" height="320" x="-75px" y="-75px" style="fill:none;stroke:white;stroke-width:150px"/>
`)
	.attr('transform','translate('+(this.rect.width/2-150)+','+(this.rect.height/2-70)+')')

    var timeline = anime.timeline({loop:true})
    var droplets =  document.querySelectorAll('#spinner .droplet')
    timeline.add({
	targets: droplets,
	duration: 3000,
	easing:'linear',
	translateX: 70
    })
	.add({
	    targets: document.querySelectorAll('#spinner #first_spin'),
	    duration: 2000,
	    fill:  [
		{value: '#C24231'},
		{value: '#C29631'},
		{value: '#e7f5f8'}
	    ],
	    easing:'linear',
	    offset: 1000
    })


    // Maximum number entries below wich scatter are drawn.
    this.max_entries_for_points = 7000

    // List wich runs are displayed.
    this.display_runs = protocol.run_list.slice()

    // Setup scales
    this.scales = {min:{}, max:{}, scale:{}, lock:{}}
    this.xaxis = svg.append('g').attr('id','xaxis')
    this.yaxis = svg.append('g').attr('id','yaxis')

    // Setup svg objects.
    this.lines_group = svg.append('g')
    this.errorbar_group = svg.append('g')
    this.scatter_group = svg.append('g')

    // Setup a list of channels
    var chan = [{name:'time',value:'time'}, {name:'train',value:'train'}]
    protocol.active_channels.forEach(function(d){
	chan.push({name: protocol.channels_names.filter(function(c){return c[0]==d})[0][1],
		   value:d})})

    // Setup axes
    this.ordinate_measure = 'median'
    this.ordinate_variable = protocol.channels_names[0][0]
    this.abscissa_measure = 'time'
    this.abscissa_variable = 'time'

    // Interface parameters
    this.selected = display_selection
    this.display_lines = true
    this.logscale = false
    this.scatter = true
    this.aggregate = false
    this.errorbar = true
    this.restrict_to_run = false
    this.restrict_to_run_nb = 1
    this.runs = protocol.run_list
    this.click_mode = 'select'

    // Mouseover text
    this.tooltip = ui.append('div').style('text-align','center').attr('class','tooltip_peaks')
    this.overtext = this.tooltip.append('div')

    this.stub_click_select_action = function (d){self.click_select_action(d)}
    this.stub_opacity_func = (d) => this.opacity_func(d)

    // Setup toolbar ui.
    ui.style('margin-top','-20px')
	.style('background-color','#f8f8f8')

    var self = this
    var toolboxes = {}
    this.buttons = {}

    toolboxes['xselect'] = ui.append('div')
	.style('display', 'inline-block')
	.style('padding-right','1em')
	.attr('id','xselect')
    toolboxes['yselect'] = ui.append('div')
	.style('display', 'inline-block')
	.style('padding-right','1em')
	.attr('id','yselect')
    toolboxes['show_hide'] = ui.append('div')
	.attr('data-placement','bottom')
	.attr('data-toggle','tooltip')
	.style('margin-right','30px')
	.style('display','inline-block')
	.attr('title','Toggle visibility of the different elements').append('div')
	.style('display','inline-block')
	.attr('class','btn-group')
	.attr('data-toggle','buttons')
	.attr('role','group')
	.style('padding-right','1em')
    toolboxes['free_btns'] = ui.append('div')
	.style('display','inline-block')
	.attr('data-toggle','buttons')
	.style('padding-right','1em')

    toolboxes['aggregate'] = ui.append('div')
	.attr('data-placement','bottom')
	.attr('data-toggle','tooltip')
	.style('float','right')
	.style('display','inline-block')
	.attr('title','Choose aggregation mode').append('div')
	.attr('class','btn-group btn-group-toggle')
	.style('display','inline-block')
	.attr('data-toggle','buttons')
	.style('padding-right','1em')
    toolboxes['click'] = ui.append('div')
	.attr('data-placement','bottom')
	.attr('data-toggle','tooltip')
	.style('float','right')
	.style('margin-right','30px')
	.style('display','inline-block')
	.attr('title','Choose click action').append('div')
	.attr('class','btn-group btn-group-toggle')
	.attr('data-toggle','buttons')
	.style('display','inline-block')


    toolboxes['run'] = ui.append('div')
	.style('display','inline-block')
	.attr('data-toggle','buttons')

    // Channel selectors
    setup_channel_selector(toolboxes.yselect,
			   'Y', chan,
			   self.ordinate_variable, self.ordinate_measure,
			   function (channel, measure){
			       self.ordinate_variable = channel
			       self.ordinate_measure = measure
			       self.update()}
			  )
    setup_channel_selector(toolboxes.xselect,
			   'X', chan,
			   self.abscissa_variable, self.abscissa_measure,
			   function (channel, measure){
			       self.abscissa_variable = channel
			       self.abscissa_measure = measure
			       self.update()}
			  )

    // Show/hide line, points, error bars...
    toolboxes['show_hide'].append('label').text('Line')
	.attr('class', 'btn btn-default'+(this.display_lines?' active':''))
	.on('click', function(){self.display_lines = !self.display_lines; self.update()})
	.append('input').attr('type','checkbox')
    this.buttons['points'] = toolboxes['show_hide'].append('label').text('Points')
	.attr('id','btn_points')
	.attr('data-toggle', 'button')
	.attr('class', 'btn btn-default')
	.on('click', function(){self.scatter = !self.scatter; self.update()})
    this.buttons['points'].append('input').attr('type','checkbox')
    this.buttons['errorbar'] = toolboxes['show_hide'].append('label').text('Std')
	.attr('class', 'btn btn-default'+(this.errorbar?' active':''))
	.on('click', function(){self.errorbar = !self.errorbar; self.update()})
    this.buttons['errorbar'].append('input').attr('type','checkbox')

    // Click
    toolboxes['click'].append('label').text('Signal')
	.on('click',function(){self.click_mode = 'signal'})
	.attr('class', 'btn btn-default form-check-label')
	.append('input').attr('type','radio').attr('name','mode')
    toolboxes['click'].append('label').text('Select')
	.on('click',function(){self.click_mode = 'select'})
	.attr('class', 'btn btn-default form-check-label active')
	.append('input').attr('type','radio').attr('name','mode')
	.property('checked',true)

    $('[data-toggle="tooltip"]').tooltip()

    // LOG scale selector
    toolboxes['free_btns'].append('label').text('Log scale')
	.attr('class', 'btn btn-default'+(this.logscale?' active':''))
	.on('click', function(){self.logscale = !self.logscale; self.update()})
	.append('input').attr('type','checkbox')


    // Toolboxes that store buttons.
    toolboxes['free_btns'].append('button')
	.attr('class', 'btn btn-default')
	.attr('type','button')
	.attr('data-toggle','modal')
	.attr('data-target','#settings')
	.text('Settings')

    // Aggregation selector
    toolboxes['aggregate'].append('label').text('Droplet')
	.on('click',function(){self.aggregate = false ; self.update()})
	.attr('class', 'btn btn-default form-check-label active')
	.append('input').attr('type','radio').attr('name','mode')
	.property('checked',true)
    toolboxes['aggregate'].append('label').text('Well')
	.on('click',function(){self.aggregate = 'well' ; self.update()})
	.attr('class', 'btn btn-default form-check-label')
	.append('input').attr('type','radio').attr('name','mode')
    toolboxes['aggregate'].append('label').text('Group')
	.on('click',function(){self.aggregate = 'group'; self.update()})
	.attr('class', 'btn btn-default form-check-label"')
	.append('input').attr('type','radio').attr('name','mode')

    // Run selector
    this.runselect = toolboxes['run'].append('div').style('display','none')
    this.runselect.append('label').text('Run: ')
    toolboxes['run'].append('label').text('Single run scatter')
	.attr('class', 'btn btn-default'+(this.restrict_to_run?' active':''))
	.on('click', function(){
	    var value = !self.restrict_to_run
	    self.restrict_to_run = value
	    self.display_only_selected = !value
	    self.display_lines = !value
	    self.scatter = value
	    self.abscissa_variable = value?'train':'time'
	    self.abscissa_measure = value?'droplet_id':'time'
	    self.runselect.style('display',value?'inline':'none')
	    d3.select("#xselect .channel_select").property('value',self.abscissa_variable)
	    setup_measure_selection(d3.select("#xselect .measure_select"), measures(self.abscissa_variable), self.abscissa_measure)
	    self.update()})
	.append('input').attr('type','checkbox')


    var ord_run = this.runselect.append('select')
    var ord_run_slider = this.runselect.append('input')
    ord_run.on('change',function(){self.restrict_to_run_nb = this.options[this.selectedIndex].value
				   ord_run_slider.node().value = this.selectedIndex
				   self.update()})
    ord_run.selectAll('option').data(this.runs).enter().append('option')
	.attr('value',function(d){return d}).text(function(d){return d})
	.property('selected', function(d,i){return self.restrict_to_run_nb?d==self.restrict_to_run_nb:i==0})

    ord_run_slider
	.attr('type', 'range').attr('min',0).attr('step',1)
	.style('display','inline').style('width','30%')
	.attr('max', ord_run.node().options.length-1)
	.attr('value', ord_run.node().selectedIndex)
	.on('change', function(){
	    ord_run.node().selectedIndex = this.value
	    self.restrict_to_run_nb = ord_run.node().options[this.value].value
	    self.update()
	})

    // Display advanced settings
    var settings = ui.append('div')
	.attr('class', 'modal fade')
	.attr('id', 'settings')
	.attr('tabindex', '-1')
	.attr('role', 'dialog')
	.attr('aria-labelledby','settingsLabel')
    $(settings.node()).on('hide.bs.modal', function () {self.update()})

    var settings_content = settings.append('div').attr('class','modal-dialog modal-lg').attr('role','document')
	.append('div').attr('class','modal-dialog modal-content').attr('role','document')
    settings_content.append('div').attr('class','modal-header')
	.html(`<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="settingsLabel">Settings</h4>`)
    var settings_body = settings_content.append('div').attr('class','modal-body')

    //// Scales options
    var scales_options = settings_body.append('div')
    scales_options.append('h3').text('Scales options')
    var stable = scales_options.append('table').attr('class','table')
    stable.append('thead').append('tr').html('<th>Name</th><th>Lock</th><th>Min</th><th>Max</th>')
    this.scales_options_form = stable.append('tbody')
    setup_scales_options(this.scales, this.scales_options_form)


    //// Marker sizes options
    marker_sizes_names = {stroke_thick:"Line width (selected)",
			  stroke_thin:"Line width",
			  circle_thin:"Circle size",
			  circle_thick:"Circle size (selected)",
			  font_size:"Font size"}
    var marker_sizes_options = settings_body.append('div')
    marker_sizes_options.append('h3').text('Marker size options')
    var div_marker_sizes = marker_sizes_options.append('form')
	.selectAll('div')
	.data(d3.keys(this.marker_sizes))
	.enter()
	.append('div').attr('class','form-group')
    div_marker_sizes.append('label').text(d=>marker_sizes_names[d]+': ')
    div_marker_sizes.append('input').attr('class','form-control')
	.attr('value', d=>this.marker_sizes[d])
	.on('change', function(d){self.marker_sizes[d]=+this.value;
				  self.update()})


    /// Run Selector
    var run_selector = settings_body.append('div')
    run_selector.append('h3').text('Run')
    var run_item = run_selector.selectAll('label').data(self.protocol.run_list).enter()
	.append('label').text(function(d){return d}).style('padding','5px')
    run_item.append('input').attr('type','checkbox')
	.property('checked', function(d){return self.display_runs.indexOf(d) != -1})
	.on('click', function(d){
	    var idx = self.display_runs.indexOf(d)
	    if (idx != -1){self.display_runs.splice(idx, 1)}
	    else { self.display_runs.push(d)}
	    self.update()})

    settings_content.append('div').attr('class','modal-footer')
	.html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>')
    $('.btn').button()
}

function setup_scales_options(scales, div){
    div.html('')
    var div_scale = div.selectAll('tr')
	.data(scales.displayed?scales.displayed:d3.keys(scales.min))
	.enter()
	.append('tr')
    div_scale.exit().remove()
    div_scale = div_scale.merge(div_scale)
    div_scale.append('th').text(d=>d)
    div_scale.append('td').append('input')
	.attr('type','checkbox')
	.property('checked', d=>scales.lock[d]!==undefined?scales.lock[d]:false)
	.on('change', function(d){scales.lock[d] = this.checked})
    div_scale.append('td').append('input')
	.attr('value',d=>scales.min[d])
	.on('change', function(d){scales['min'][d]=+this.value;
				  scales.lock[d]=true; setup_scales_options(scales,div)})
    div_scale.append('td').append('input')
	.attr('value',d=>scales.max[d])
	.on('change', function(d){scales['max'][d]=+this.value;
				  scales.lock[d]=true; setup_scales_options(scales,div)})
}

function get_channel(variable, cnames){
    return cnames.filter(function(c){return c[0]==variable})[0][1]
}

function get_column(variable, measure, cnames){
    var col
    if (variable == 'time'|| variable=='train'){
	col = measure
    }else{
	col = get_channel(variable, cnames)
	col += '_' + measure
    }
    return col
}

Dynamics.prototype.update = function () {
    // Hide spinner
    this.spinner.attr('opacity',1)

    // Update URL
    var new_URL = setParameterByName(window.location.href,'ordV',this.ordinate_variable)
    new_URL = setParameterByName(new_URL,'ordM',this.ordinate_measure)
    window.history.replaceState( {} , '', new_URL)

    ///// BUILD THE AJAX QUERY /////
    // Get the columns we want
    var ycol = get_column(this.ordinate_variable, this.ordinate_measure, this.protocol.channels_names)
    var xcol = get_column(this.abscissa_variable, this.abscissa_measure, this.protocol.channels_names)
    var query = {columns:['droplet_id', 'time', 'id_exp', 'well', 'run', 'group', ycol, xcol]}

    // Filter the runs
    if (this.protocol.run_list.length != this.display_runs.length){query['runs'] = this.display_runs}
    if (this.restrict_to_run) {query['runs'] = [this.restrict_to_run_nb]}

    // Add the aggregation option
    if (this.aggregate != false){query['aggregate'] = this.aggregate}

    // Filter the droplets
    var id_exp_query = Object.keys(this.selected.selected).filter(d=>this.selected.selected[d])
    if (id_exp_query.length != this.selected.droplets.length){
	query['id_exp'] = id_exp_query
    }

    var self = this
    //// Request data and update display ////
    console.log('AJAX QUERY:', query)
    this.current_request = JSON.stringify(query)
    if (this.current_request != this.last_request){
	$.ajax({ type: 'POST',
		 url: 'data',
		 timeout:0,
		 data: this.current_request,
		 contentType: 'application/json',
		 dataType: 'json'})
	    .error(function(xhr, status, err) {
		console.error(status, err.toString())
		console.error('XHR is', xhr)
		d3.select('main').html(xhr.responseText)
	    })
	    .fail(function(err) {
		console.error('Ajax Request -- Error', err)
	    })
	    .always(function(data,err){self.data=data
				       self.draw(xcol, ycol)})
    } else{
	console.log('No new request, using cached data.')
	self.draw(xcol, ycol)
    }
    this.last_request = this.current_request
}

Dynamics.prototype.draw = function draw(xcol, ycol){
    var data = this.data
    var self = this
    console.log('Drawing data with '+ data.length+' entries.')

    var aggregate_on = 'id_exp'
    var are_dots_drawn = (this.scatter && data.length < this.max_entries_for_points)

    // Identify variables
    var yvar = ycol
    var xvar = xcol
    var yerr

    if (self.aggregate != false){
	xvar = (xvar != 'run') ? xcol+'_mean' : xcol
	yvar = ycol+'_mean'
	yerr = ycol+'_std'
	aggregate_on = self.aggregate
    }

    if (this.abscissa_measure == 'train'){xvar='id_exp'}
    if (this.ordinate_measure == 'train'){yvar='id_exp'}

    // Update ui
    this.buttons.errorbar.attr('class', 'btn btn-default'+(aggregate_on == 'id_exp'? ' disabled' : ''))
    this.buttons.points.attr('class', 'btn btn-default'+(self.scatter?' active':'') + (data.length<this.max_entries_for_points ? '' : ' disabled'))

    // Update scales
    function  update_scale(v){
	// If scales is unlocked, dynamically set the domain, otherwise keep it.
	if (self.scales.lock !== undefined && !self.scales.lock[v]){
	    self.scales.min[v] = d3.min(data, function(d) {return d[v]})
	    self.scales.max[v] = d3.max(data, function(d) {return d[v]})
	    var range = Math.abs(self.scales.max[v]-self.scales.min[v])
	    self.scales.min[v] = Math.max(0, self.scales.min[v] - 0.05 * range)
	    self.scales.max[v] = self.scales.max[v] + 0.05 * range
	}
    }
    update_scale(xvar)
    update_scale(yvar)
    self.scales['displayed'] = [xvar, yvar]
    setup_scales_options(self.scales, self.scales_options_form)

    var y
    if (this.logscale){
	y = d3.scaleLog()
	y.clamp(true)
	y.domain([d3.min(data, function(d) {return d[yvar]>0 ? d[yvar] : undefined}),
		  this.scales.max[yvar]])
    } else{
	y = d3.scaleLinear()
	y.domain([this.scales.min[yvar], this.scales.max[yvar]])
    }
    var margin_y = 30*self.marker_sizes['font_size']
    y.range([this.rect.height-margin_y,5])

    var x = d3.scaleLinear()
	.range([30,this.rect.width-30])
	.domain([this.scales.min[xvar], this.scales.max[xvar]])

    // Update axes
    var xAxis = d3.axisBottom().scale(x).ticks(10)
    var yAxis = d3.axisRight(y)
	.tickSize(Math.abs(x.range()[0]-x.range()[1]))
    if (xvar=='time'||xvar=='time_mean')  {xAxis.tickFormat(timeFormat)}

    this.xaxis
	.attr('class', 'x axis')
	.attr('transform', 'translate(0,'+(this.rect.height-margin_y+10)+')')
	.style('font-size', this.marker_sizes.font_size+'em')
	.call(xAxis)
    this.yaxis
	.attr('class', 'y axis')
	.attr('transform', 'translate(30,0)')
	.style('font-size', this.marker_sizes.font_size+'em')
	.call(yAxis)
    this.yaxis.select('.domain').remove()
    this.yaxis.selectAll('.tick line')
	.attr('stroke', '#777')
	.attr('stroke-width', '0.3')
    this.yaxis.selectAll('.tick text')
	.attr('x', 4)
	.attr('dy', -4)

    // Plot lines
    var nested = d3.nest().key(function(d){return d[aggregate_on]}).entries(data)

    nested.forEach(function(d){
	d.well = d.values[0].well
	d.group = d.values[0].group? d.values[0].group : self.protocol.well_to_group[d.well]
	d.color = self.protocol.color_group[d.group]
	d.id_exp = d.values[0].id_exp
	d.opacity = self.stub_opacity_func(d.values[0].id_exp)
	d.droplet_id = d.values[0].droplet_id
    })

    data.forEach(function(d){
	d.group = d.group ? d.group : self.protocol.well_to_group[d.well]
	d.color = self.protocol.color_group[d.group]
	d.opacity = self.stub_opacity_func(d.id_exp)
    })

    var posx = function(d){return x(d[xvar])}
    var posy = function(d){return y(d[yvar])}
    var poserry_min = function(d) {return y(d[yvar]-d[yerr])}
    var poserry_max = function(d) {return y(d[yvar]+d[yerr])}
    var line = d3.line().x(posx).y(posy)
    var lines = display_lines(this.lines_group, this.display_lines?nested:[], line,
			      this.tooltip, this.overtext, this.marker_sizes)
    var dots = display_dots(this.scatter_group, posx, posy, are_dots_drawn?data:[],
			    this.display_lines, this.tooltip, this.overtext, xvar, yvar,
			    this.marker_sizes)
    display_errorbars(this.errorbar_group, posx, poserry_min, poserry_max,
		      (yerr!=undefined & this.errorbar==true) ? data : [],
		      this.marker_sizes)

    // Interactivity
    lines.on('click', function(d){
	if (self.click_mode == 'select'){
	    self.stub_click_select_action(d.id_exp)
	}})
    dots.on('click', function(d){
	if (self.click_mode == 'signal'){
	    if(d.id_exp){
		var chan = get_channel(self.ordinate_variable, self.protocol.channels_names)
		var target = 'explore_signal?drop='+d.id_exp+'&run='+d.run+'&channel='+chan
		console.log(target, d)
		if (d3.event.ctrlKey){
		    window.location.assign(target, '_blank')
		}
		else{
		    window.open(target)
		}}}
	else if (self.click_mode == 'select'){
	    self.stub_click_select_action(d.id_exp)
	}
    })

    setup_dragAndDrop(self.svg, function select(x0,x1,y0,y1) {
	var xmin = Math.min(x.invert(x0),x.invert(x1)),
	    xmax = Math.max(x.invert(x0),y.invert(x1)),
	    ymin = Math.min(y.invert(y0),x.invert(y1)),
	    ymax = Math.max(y.invert(y0),y.invert(y1))
	var selection = data.filter(function(d){
	    return (d[xvar] > xmin) && ( d[xvar] < xmax) && (d[yvar] > ymin) && (d[yvar] < ymax)})
	self.stub_click_select_action(selection.map(function(d){return d.id_exp}))
    })

    // Hide the loading spinner.
    self.spinner.attr('opacity',0)
}


function tooltip_move(tooltip) {
    return function(){
	var bb = tooltip.node().getBoundingClientRect()
	tooltip.style('top', Math.min($(window).height()-bb.height, (d3.event.layerY + 10)) + 'px')
	    .style('left', Math.min($(window).width()-bb.width, (d3.event.layerX + 10)) + 'px')
    }
}
function tooltip_remove(tooltip) {
    return function(){tooltip.style('display', 'none')}
}

function setup_measure_selection(select_object, data, measure){
    var select = select_object.selectAll('option')
	.data(data)
    select.enter()
	.append('option')
	.merge(select)
	.property('selected', function(d){return d==measure})
	.attr('value',function(d){return d}).text(function(d){return d})
    select.exit().remove()
}

function measures(channel){return channel == 'time' ? ['time', 'run'] : channel == 'train' ? ['droplet_id', 'id_exp', 'id_run'] : ['max','median','cov','area','mean']}

function setup_channel_selector(div, title, data, channel, measure, update){
    div.append('label').text(title+': ')
    var channel_select = div.append('select').attr('class','channel_select')
    var measure_select = div.append('select').attr('class','measure_select')

    channel_select.selectAll('option')
	.data(data)
	.enter()
	.append('option')
	.property('selected', function(d){return d.value==channel})
	.attr('value', function(d){return d.value})
	.text(function(d){ return d.name })

    measure_select.on('change',function(){
	measure = this.options[this.selectedIndex].value
	update(channel, measure)})

    channel_select.on('change',function(){
	channel = this.options[this.selectedIndex].value
	var mc = measures(channel)
	measure = (mc.indexOf(measure)!=-1)?measure:mc[0]
	setup_measure_selection(measure_select, mc, measure)
	update(channel, measure)})
    setup_measure_selection(measure_select, measures(channel), measure)
}

function display_errorbars(g, x, errymin, errymax, data, marker_sizes){
    var error_bar = g.selectAll('.errorbar')
	.data(data)
    error_bar.exit().remove()
    error_bar.enter().append('line')
	.attr('class', 'errorbar')
	.merge(error_bar)
	.attr('stroke-width', marker_sizes.stroke_thin)
	.attr('stroke', function(d){return d.color})
	.attr('x1', x)
	.attr('x2', x)
	.attr('y1', errymin)
	.attr('y2', errymax)
}

function display_dots(g, xfunc, yfunc, data, full, tooltip, overtext, xvar, yvar,  marker_sizes){
    var small_dots = g.selectAll('.dot')
	.data(data)
    var dots = small_dots.enter().append('circle')
	    .attr('class','dot')
	    .merge(small_dots)
	    .attr('r', d => d.opacity? marker_sizes.circle_thick: marker_sizes.circle_thin)
	    .attr('fill', full ? 'white' : function(d){return d.color})
	    .attr('stroke', d => d.opacity?'black':d.color)
	    .on('mouseover', function(d){
		var span_open = ' <span class="label" style="margin-right:.1em;background-color:'+d.color+'"> '
		var span_close = ' </span> '
		var fmt = d3.format('.5g')
		tooltip.style('display', 'block')
		overtext.html((d.id_exp?(span_open + 'Droplet: ' + d.droplet_id + span_close):'')
			      +(d.well?(span_open + 'Well: ' + d.well + span_close+'<br/>'):'')
			      +(d.group?(span_open + d.group + span_close):'')
			      +'<br/>'
			      +'<table  style="width:100%" class="table-condensed table-bordered">'
			      +'<th>Value</th><td> '+fmt(d[yvar])+'</td></tr>'
			      +'<th>Time</th><td> '+timeFormat(d[xvar])+'</td></tr>'
			      +'<th>Run</th><td> '+d.run+'</td></tr>'
			      +'</table>'
			     )})
	    .on('mouseout', tooltip_remove(tooltip))
	    .on('mousemove', tooltip_move(tooltip))
	    .attr('cx', xfunc)
	    .attr('cy', yfunc)
    small_dots.exit().remove()
    return dots
}


function display_lines(g, data, line, tooltip, overtext, marker_sizes){
    var lnes = g.selectAll('path')
	.data(data)
    lnes.exit()
	.remove()
    var selection = lnes.enter()
	    .append('path')
	    .attr('stroke-linejoin', 'round')
	    .attr('stroke-linecap', 'round')
	    .attr('fill', 'none')
	    .merge(lnes)
	.attr('stroke-width', d => d.opacity ? marker_sizes.stroke_thick : marker_sizes.stroke_thin)
	    .attr('opacity', d => d.opacity?1:0.5)
	    .attr('stroke', function(d){return d.color})
	    .attr('d', function(d){return line(d.values)})
	    .on('mouseover', function(d){
		var span_open = ' <span class="label" style="margin-right:.1em;background-color:'+d.color+'"> '
		var span_close = ' </span> '
		tooltip.style('display', 'block')
		overtext.html((d.droplet_id?(span_open + 'Droplet: ' + d.droplet_id + span_close):'')
			      +(d.well?(span_open + 'Well: ' + d.well + span_close+'<br/>'):'')
			      +(d.group?(span_open + d.group + span_close):''))})
	    .on('mouseout', tooltip_remove(tooltip))
	    .on('mousemove', function() {
		tooltip.style('top', (d3.event.layerY + 10) + 'px')
		    .style('left', (d3.event.layerX + 10) + 'px')
	    })
    return selection
}



function setup_dragAndDrop(svg, callback){
    var selecting = false
    svg.on('mousedown', function(){
	var p = d3.mouse(this)
	selecting = true
	svg.append('rect').attr('class', 'selection')
	    .style('fill', 'gray')
	    .style('opacity', .3)
	    .style('stroke', 'gray')
	    .attr('x0', p[0]).attr('y0', p[1])
	    .attr('x', p[0]).attr('y', p[1])
    })
    svg.on( 'mousemove', function() {
	if (selecting){
	    var s = svg.select('rect.selection')
	    if( !s.empty()){
		var p = d3.mouse(this)
		// TODO: There is a bug if the rect is started from the bottom
		s.attr('x', Math.min(s.attr('x0'), p[0]))
		s.attr('y', Math.min(s.attr('y0'), p[1]))
		s.attr('width', Math.abs(p[0] - s.attr('x0')))
		s.attr('height', Math.abs(p[1] - s.attr('y0')))
	    }
	}
    })
    d3.select('body').on('mouseup', function(){
	selecting = false
	svg.select('rect.selection').remove()
    })
    svg.on('mouseup', function(){
	if (selecting){
	    selecting = false
	    var s = svg.select('rect.selection')
	    if(!s.empty()){
		var p = d3.mouse(this)
		callback(p[0],s.attr('x0'),p[1],s.attr('y0'))
	    }
	    s.remove()
	}
    })
}

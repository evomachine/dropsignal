// protocol_editor.js - An interactive way of editing the protocol file.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---



$(window).ready(function(){
    var control = d3.select('#control')

    function reset(){
	var form = d3.select('#protocol_form')
	form.select('#form_action').attr('value','reset')
	form.node().submit()
    }
    d3.select('#control_foot').append('span')
	.attr('class','btn btn-danger')
	.text('Reset to default')
	.on('click', reset)

    var raw_div = d3.select('#detect')
    var threshold_input = d3.select('#edit_detection_threshold input').on('change',request)
    var window_input = d3.select('#edit_window input').on('change',request)
    var window_size_input = d3.select('#edit_window_size input').on('change',request)
    var buffer_drops_max_number_input = d3.select('#edit_buffer_drops_max_number input').on('change',request)
    var buffer_drops_waiting_time_input = d3.select('#edit_buffer_drops_waiting_time input').on('change',request)
    var advanced_parameters_input = d3.select('#edit_advanced_detection_parameters input').on('change',request)
    var active_channels_input = d3.select('#edit_active_channels input').on('change',request)

    var preview_on_runs = 3
    raw_div.append('h4').text('Detection preview')
    raw_div.attr('class','alert alert-info')
    var form = raw_div.append('form')
    form.append('label').text('Number of Runs: ')
    form.append('input')
	.attr('type','number')
	.attr('value',preview_on_runs)
	.on('change',function(){
	    preview_on_runs = this.value
	    request()
	})
    var out_div =  raw_div.append('div')

    function request(){
	var advanced_detection_parameters = JSON.parse(advanced_parameters_input.property('value'))
	var active_channels = JSON.parse(active_channels_input.property('value'))

	var thres = threshold_input.property('value')
	var window = window_input.property('value').replace(/['"]+/g, '')
	var window_size = window_size_input.property('value')
	var buffer_drops_max_number =  buffer_drops_max_number_input.property('value')
	var buffer_drops_waiting_time =  buffer_drops_waiting_time_input.property('value')

	var out = 'threshold:'+thres+' window: '+window+' window_size: '+window_size

	out += ' buffer_drops_waiting_time: '+buffer_drops_waiting_time+' buffer_drops_max_number: '+buffer_drops_max_number+'<br/>'
	d3.keys(advanced_detection_parameters).forEach(d=>{out+=d+' with advanced parameters: '+JSON.stringify(advanced_detection_parameters[d])+'<br/>'})
	var uri_root = '/data/detect/?threshold='+thres+'&window='+window+'&window_size='+window_size
	uri_root += '&buffer_drops_waiting_time='+buffer_drops_waiting_time+'&buffer_drops_max_number='+buffer_drops_max_number
	uri_root += '&advanced='+JSON.stringify(advanced_detection_parameters)+'&active_channels='+JSON.stringify(active_channels)
	var uri_0 = uri_root+'&run=0&ptd=ptd4'
	d3.json(uri_0, function(error, json){
	    console.log('request', uri_0, error, json)
	    out += '<div class="run">'
	    if (error) { out += 'Head detection failed'
			 out += '<div style="height:100px; overflow:scroll">'+error.explicitOriginalTarget.response+'</div>'
			 }
	    else if (json.error != undefined ){out += 'Head detection failed: '+json.error}
	    else{
		out += '<strong>Run '+json.request.run+': </strong>'
		out += '<span class="label label-primary">Head:'+json.ptd4.number+' droplets </span>'
	    }
	    out += '</div>'
	    out_div.html(out)})

	for (var run = 1; run<preview_on_runs; run++){
	    var uri = uri_root+'&run='+run
	    d3.json(uri, function(error, json){
		console.log('request', uri, error, json)
		out += '<div class="run">'
		if (error) { out += 'detection failed'
			     out += '<div style="height:100px; overflow:scroll">'+error.explicitOriginalTarget.response+'</div>'
			   }
		else if (json.error != undefined ){out += 'Run '+json.request.run+' detection failed: '+json.error}
		else{
		    out += '<strong>Run '+json.request.run+': </strong>'
		    json.ptd.forEach( d=> {
			out += '<span class="label label-primary">'+d+(json[d].advanced?'*':'')+': '+json[d].number+' droplets </span>'
		    })
		}

		out += '</div>'
		out_div.html(out)
	    })
	}
}
})

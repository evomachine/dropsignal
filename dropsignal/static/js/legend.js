// legend.js - Write group color legend.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function Legend(div, droplets, group_to_color){
    // Add a line per group.
    var grp = div.append('ul').selectAll('li')
	.data(droplets)
	.enter().append('li')

    // this.click_group will be defined in main_dynamics.js
    this.stub_click_group = d => this.click_group(d)

    grp.append('span')
	.text(function(d){return d.label +' ('+d.id.length+' well'+(d.id.length>1?'s':'')+')' })
	.attr('class','label')
	.style('background-color',function(d){return group_to_color[d.label]})
	.on('click', this.stub_click_group)
}

Legend.prototype.update = function (){

}

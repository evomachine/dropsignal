// main_dynamics.js
// This file is part of the dropsignal package.
// Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

// Dependencies
var d3 // from d3.js
var $ // from jquery
var Plate // from plate.js
var DropletSelector // from selector.js
var Legend // from legend.js
var Dynamics // from dynamics.js
var DropletSelection // from selection.js

///////////////////////////
// INIT GLOBAL VARIABLES //
///////////////////////////

// Widgets sizes
var W = (19.5/20) * $(window).width(), H = 600
var W_plate = 450, H_plate = 300

// List of droplets to select
var droplets

// Sepecial selection holding the droplets that are to
// be displayed in dynamics.
var display_selection

// Widgets are put in global variables
var selector
var plate
var legend
var dynamics

// DEBUG
var _protocol

d3.queue()
    .defer(d3.json, 'metadata')
    .await(function(error, protocol) {

	if (error){
	    var hud = d3.select('main').append('div')
	    hud.attr('class','alert alert-danger')
	    hud.html('<strong> Error in the metadata request: </strong> <div style="height:200px; overflow:scroll">'+error.explicitOriginalTarget.response+'</div>')
	}

	// DEBUG
	_protocol = protocol
	console.log('Protocol: ', _protocol)

	// Setup the svg.
	d3.select('#main_graph')
	    .attr('width',W)
	    .attr('height',3*H/4)
	    .style('margin-left','10px')

	// Prepare the list of droplets.
	droplets = []
	for (var id_exp in protocol.droplet_to_well) {
	    var well = protocol.droplet_to_well[id_exp]
	    if (well != 'Unknown'){
		droplets.push({id_exp: +id_exp,
			       droplet_id: +id_exp+1,
			       well: protocol.droplet_to_well[id_exp],
			       group:protocol.well_to_group[protocol.droplet_to_well[id_exp]],
			       selected: false})
	    }
	}

	// Mapping used to export the sorting list
	var sorting_drops = {
	    "run": protocol.last_complete_coalescence_information_run,
	    "mapping": protocol.sorting_mapping_id_exp_to_id_run
	}

	// Keep a mapping from well to well color.
	var well_color = {}
	droplets.forEach(function(drop){well_color[drop.well] = protocol.color_group[drop.group]})

	// Init the display selection, and select all.
	display_selection = new DropletSelection(droplets).all()

	// Setup the widgets
	legend = new Legend(d3.select('#legend'), protocol.droplets, protocol.color_group)
	plate = new Plate(d3.select('#plate').append('svg'), W_plate, H_plate, droplets, well_color, true)
	dynamics = new Dynamics(d3.select('#main_graph'), d3.select('#control'), protocol, droplets, display_selection)
	selector = new DropletSelector(droplets, d3.select('#segments'), protocol.sorting_drops, display_selection, sorting_drops)

	// Bind the widget actions.
	function update_display(){console.log('>>>>> Updating display...');
				  dynamics.update();
				  plate.update();
				  selector.update()}
	plate.update_display = update_display
	selector.update_displays = update_display
	dynamics.opacity_func = d => selector.current_selection.selected[d]
	dynamics.click_select_action = d => {selector.current_selection.select_droplet(d); update_display()}
	plate.get_current_selection = mode => mode == 'display' ? display_selection : selector.current_selection
	legend.click_group = d => {display_selection.select_group(d.label);  update_display()}
	update_display()
    })

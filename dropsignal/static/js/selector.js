// selector.js - Droplet selector object.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function DropletSelection(droplets, name){
    // A droplet selection is defined by:
    // sel.selected: is the drop selected: a boolean for each id_exp
    // sel.segment_mode: should the drop be bundled with neighbors during sorting: a boolean for each id_exp
    // sel.drops: (int) gives how many drops are selected.
    // sel.name: (str) name of the selection
    this.drops = 0
    this.name = name
    this.selected = {}
    this.segment_mode = {}
    this.droplets = droplets
    droplets.forEach(d => {this.selected[d.id_exp] =  false
			   this.segment_mode[d.id_exp] = true})
}

DropletSelection.prototype.select_droplet = function(id_exp, force){
    // Select a droplet by id_exp or list of id_exp.
    // Set the selection to `force`, toggle selection if force is undefined.
    var action = force!=undefined ? value => force : value => !value
    if (id_exp instanceof Array){
	this.droplets
	    .filter(d => id_exp.indexOf(d.id_exp)>=0)
	    .forEach(d => {this.selected[d.id_exp] = action(this.selected[d.id_exp])} )
    }
    else{
	var elt = this.droplets.filter(function(d){return d.id_exp==id_exp})[0]
	this.selected[elt.id_exp] = action(this.selected[elt.id_exp])
    }
    this.update_dropcount()
}

DropletSelection.prototype.toggle_set = function(droplets){
    // If all the droplets in the droplet set are selected, unselect everything.
    // Otherwise, select everything
    var selected_from_this_group = d3.sum(droplets, d => this.selected[d.id_exp])
    var new_value = selected_from_this_group !=  droplets.length
    droplets.forEach(d => this.selected[d.id_exp]=new_value)
    var selecting = (new_value ? droplets.length-selected_from_this_group: -selected_from_this_group)
    this.drops = this.drops + selecting
}

// Convenience functions to select from a well, group, row or column.
DropletSelection.prototype.select_well = function(well){
    this.toggle_set(this.droplets.filter(d => d.well==well))}
DropletSelection.prototype.select_group = function(group){
    this.toggle_set(this.droplets.filter(d => d.group==group))}
DropletSelection.prototype.select_column = function(column){
    this.toggle_set(this.droplets.filter(d => d.well.substring(1)==column))}
DropletSelection.prototype.select_row = function(row){
    this.toggle_set(this.droplets.filter(d => d.well[0]==row))}

DropletSelection.prototype.update_dropcount = function(){
    this.drops = d3.sum(Object.keys(this.selected), key => this.selected[key])}

// Select all, select none.
DropletSelection.prototype.clear = function(){
    this.droplets.forEach(d => this.selected[d.id_exp]=false); this.drops=0; return this}
DropletSelection.prototype.all = function(){
    this.droplets.forEach(d => this.selected[d.id_exp]=true); this.update_dropcount(); return this}

// Set the segment mode for all droplets.
DropletSelection.prototype.set_segment_mode = function(mode){
    this.droplets.forEach(d => this.segment_mode[d.id_exp]=mode); return this}

/////////////////////////////////////////////////////////////////////////////////////////////////

function DropletSelector(droplets, div, last_run_drops, display_selection, sorting_drops){
    this.div = div
    this.droplets = droplets
    this.update_displays = function(){}
    this.sorting_drops = sorting_drops

    this.last_run_drops = last_run_drops
    this.current_selection = new DropletSelection(droplets)
    this.saved_selections = []

    // Set the save UI
    var self = this
    this.selection_description = this.div.append('div')
    this.div.append('button')
	.text('Save')
	.on('click', () => this.save(undefined))
	.attr('class','btn btn-default')
    this.div.append('button')
	.text('Clear')
	.on('click', () => {this.current_selection.clear(); this.update_displays()})
	.attr('class','btn btn-default')
	.attr('data-placement','bottom')
	.attr('data-toggle','tooltip')
	.attr('title','Select no droplet')

    this.div.append('button')
	.text('All')
	.on('click', () => {this.current_selection.all(); this.update_displays()})
	.attr('class','btn btn-default')
	.attr('data-placement','bottom')
	.attr('data-toggle','tooltip')
	.attr('title','Select all droplets')

    this.div.append('button')
	.text('Get from display')
	.on('click', () => {
	    this.current_selection.selected = Object.assign({}, display_selection.selected)
	    this.current_selection.update_dropcount()
	    this.update_displays()})
	.attr('class','btn btn-default')
	.attr('data-placement','bottom')
	.attr('data-toggle','tooltip')
	.attr('title','Select displayed droplets only')

    this.div.append('button')
	.text('Set as display')
	.on('click', () => {
	    display_selection.selected = Object.assign({}, this.current_selection.selected)
	    display_selection.update_dropcount()
	    this.update_displays()})
	.attr('class','btn btn-default')
	.attr('data-toggle','tooltip')
	.attr('data-placement','bottom')
	.attr('title','Display selected droplets only')

    var group_select_by_id = this.div.append('div')
	.attr('class', 'form-inline')
	.append('span')
	.style('border', '1px solid #cccccc')

    // Manually add or remove a drop form the selection.
    var offset = 1
    var drop_id_input = group_select_by_id.append('input')
	.attr('class', 'form-control')
	.attr('type','number')
	.on('keyup', d=>{if (d3.event.keyCode == 13) {
	    this.current_selection.select_droplet(drop_id_input.property("value")-offset, true)
	    this.update_displays()
	}})
    group_select_by_id.append('button').text('+')
	.attr('class', "btn btn-outline-secondary")
	.attr('type', "button")
	.attr('data-toggle','tooltip')
	.attr('data-placement','bottom')
	.attr('title','Add a single droplet')
	.on('click', d=>{this.current_selection.select_droplet(drop_id_input.property("value")-offset,
							       true)
			 this.update_displays()})
    group_select_by_id.append('button').text('-')
	.attr('class', "btn btn-outline-secondary")
	.attr('type', "button")
	.on('click', d=>{this.current_selection.select_droplet(drop_id_input.property("value")-offset,
							       false)
			 this.update_displays()})
	.attr('data-toggle','tooltip')
	.attr('data-placement','bottom')
	.attr('title','Remove a single droplet')

    // Set the export UI
    this.div.append('h4').text('Sorting Informations').style('margin-top','1em')

    var form = this.div.append('form')
	.attr('class','form-group')
	.style('display','inline')

    this.div_status = this.div.append('div')
    this.div_status.append('span')
    this.div_status.append('p')

    this.div_segments = this.div_status.append('div')
	.style('border', '1px solid #cccccc')
	.style('margin', '0.5em')
	.style('align', 'center')

    this.div_list = this.div_segments.append('div')
	.style('margin', '0.5em')

    var buttons_segments = this.div_segments.append('div')
	.style('margin','auto')
	.style('align','center')

    buttons_segments.append('button')
	.text('All individual')
	.on('click', () => {this.current_selection.set_segment_mode(false);
			    this.update_displays()})
	.attr('class','btn btn-default')
	.style('display', 'inline-block')
	.attr('data-toggle','tooltip')
	.attr('data-placement','left')
	.attr('title','Separate all drops \n Click droplets or segments to toggle their mode')

    buttons_segments.append('button')
	.text('All segments')
	.on('click', () => {this.current_selection.set_segment_mode(true);
			    this.update_displays()})
	.attr('class','btn btn-default')
	.style('display', 'inline-block')
	.attr('data-toggle','tooltip')
	.attr('data-placement','left')
	.attr('title','Pool all contiguous drops \n Click droplets or segments to toggle their mode')

    this.a_download = buttons_segments.append('a')
	.text('Export')
	.attr('class','btn btn-default')
	.attr('target','_blank')
	.attr('download','sorting.dat')
	.style('display', 'inline-block')
	.attr('data-toggle','tooltip')
	.attr('data-placement','left')
	.attr('title','Export the sorting list')


    // Saved selections
    this.div.append('h3').text('Saved selections')
    this.save_buttons_ui = this.div.append('div')
    this.save_list_ui = this.div.append('div')
}

DropletSelector.prototype.update_save_ui = function(){
    var selection = this.save_list_ui.selectAll('div')
	.data(this.saved_selections)
    var self = this
    selection.exit().remove()
    var merged = selection.enter()
	.append('div')
	.merge(selection)
	.html('')
    var ip = merged.append('input').attr('value',function(d){return d.name}).on('input', function(d){d.name = this.value})
    merged.append('button').text('Load').on('click',function(d,i){self.load(i)}).attr('class','btn btn-default')
    merged.append('button').text('Delete').on('click',function(d,i){self.remove(i)}).attr('class','btn btn-default')
    merged.append('span').text(function(d){return d.drops + ' droplet'+(d.drops>0?'s':'')}).attr('class', 'label label-default').style('margin-left','1em')
}

DropletSelector.prototype.save = function(name){
    if (name == undefined) {name = 'selection '+(this.saved_selections.length+1)}
    var selection = new DropletSelection(this.droplets, name)
    selection.selected = Object.assign({}, this.current_selection.selected)
    selection.segment_mode = Object.assign({}, this.current_selection.segment_mode)

    selection.update_dropcount()
    this.saved_selections.push(selection)
    this.update_save_ui()
}

DropletSelector.prototype.remove = function(number){
    this.saved_selections.splice(number,1)
    this.update_save_ui()
}

DropletSelector.prototype.load = function(number){
    this.current_selection.selected = Object.assign({}, this.saved_selections[number].selected)
    this.current_selection.segment_mode = Object.assign({}, this.saved_selections[number].segment_mode)
    this.current_selection.update_dropcount()
    this.update_displays()
}

DropletSelector.prototype.update = function(){
    // // Update the selector and its UI.
    this.segments = build_segments(this.current_selection)

    // // Check the segment list.
    var check = check_segments(this.segments)
    var selected = this.segments.filter(function(d){return d.type=='selected'})
    var seg_text = ' in '+selected.length+' sorting well'+(selected.length>0?'s':''+'.')
    var droplets_text = this.current_selection.drops+' droplet'+(this.current_selection.drops>0?'s':'')
    this.selection_description.text(droplets_text + seg_text)
    this.div_status.attr('class', 'alert'+(check[0]?' alert-success':' alert-warning'))
    this.div_status.select('span').html('<strong>'+(check[0]?'Valid sorting':'Sorting might be invalid:')+' </strong>'+check[1])
    var exported = export_sorting_list(this.segments, this.sorting_drops.mapping)
    this.div_status.select('p').text('Last run with complete coalescence information ('+this.sorting_drops.run+') will be used for the sorting.')
	.append('p').text(exported[1] ? 'Coalesced droplet selected':' ')
    this.a_download.attr('href', 'data:attachment/text,' + encodeURI(exported[0]))
    display_segments(this.div_list, this.segments, this.current_selection, d=>this.update())
}

function display_segments(div, segments, selection, callback){
    // Display the segments from a selection as badges (black for
    // segments and white for individual droplets).  You can toggle
    // the segment mode of one drop (to segments) or a segment of
    // drops (to individuals) by clicking On the badge.
    div.html('')
    var offset = 1
    var data = segments.filter(d=>d.type=='selected')
    if (data.length == 0){
	div.text('Selection is empty')}
    div.selectAll('div')
	.data(data)
	.enter()
	.append('div')
	.text(d=>(d.start+offset)+(d.len!=1?' to '+(d.start+offset+d.len-1):''))
	.attr('class', "badge")
	.style('background-color', d=>selection.segment_mode[d.start]?"#343a40":"#ffffff")
	.style('color', d=>selection.segment_mode[d.start]?"#ffffff":"#343a40")
	.style('border', d=>selection.segment_mode[d.start]?"":"1px solid #343a40")
	.on('click', d=>{
	    var current = selection.segment_mode[d.start]
	    for (let i=0; i<d.len; i++){
		selection.segment_mode[d.start+i] = ! current
	    }
	    callback()
	})
}

function build_segments(selection){
    // Build the segment list.
    var current = false
    var current_segment_mode = false
    var previous = false
    var previous_segment_mode = false
    var seg = []
    var len = 0
    var start = undefined
    var stop = false
    // Loop through droplets
    selection.droplets.forEach(function(d){
	current = selection.selected[d.id_exp]
	current_segment_mode = selection.segment_mode[d.id_exp]

	// Should we stop the segment ?
	if (start === undefined){
	    // We do not do it at the first drop...
	    start = d.id_exp
	} else {
	    // We only do it if the selection status changes
	    // or if the status is the same and one of the two
	    // focal drop is not in segment mode.
	    stop = ((current != previous) ||
		    (current && previous && !(previous_segment_mode && current_segment_mode)))
	}

	// Stop the segment if required.
	if (stop){
	    seg.push({start:start,
		      len:len,
		      type: previous ? 'selected':'waste'})
	    len = 0
	    start = d.id_exp
	}
	len++
	previous = selection.selected[d.id_exp]
	previous_segment_mode = selection.segment_mode[d.id_exp]
    })

    // Last segment
    seg.push({start:start,
	      len:len,
	      type: previous ? 'selected':'waste'})

    return seg
}

function check_segments (seg){
    // Check wether the segments form a valid sorting.
    var s = 0
    var w = 0
    var sum_w_len = 0
    var sum_w_fill = 0
    var mx_s = 0
    seg.forEach(function(d){
	if (d.type == 'selected'){
	    s++
	    mx_s = Math.max(mx_s,d.len)
	}
	else{
	    w++
	    sum_w_len += d.len
	    sum_w_fill += Math.floor((d.len-1)/50)
	}
    })
    if (mx_s > 50){
	return [false,'Selection segment too long: '+mx_s]
    }
    if (s+w+sum_w_fill>95){
	return [false, 's + w + sum((W_i-1)/50) > 95']}
    if (s+sum_w_len<95){
	return [false, 's + sum(W_i) < 95']}
    return [true, '']
}

function export_sorting_list(segments, mapping){
    // Export the selection as a list for the sorter.
    var txt = ''
    var coal = false
    var offset = 1 // The machine wants 1-based indexing.
    segments.forEach(function(d) {
	if (d.type=='selected'){
	    var id_run_start = mapping.filter(e=>+e[0]==d.start)
	    id_run_start = id_run_start.length ? +id_run_start[0][1] : undefined

	    var id_run_end = mapping.filter(e=>+e[0]==d.start+d.len-1)
	    id_run_end = id_run_end.length ? +id_run_end[0][1] : undefined

	    if (id_run_start == undefined){
		if (d.len > 1){
		    id_run_start = +mapping.filter(e=>+e[0]==d.start+1)[0][1]
		    txt= txt +(id_run_start+offset)+(d.len!=1?' '+(id_run_end+offset):'')+'\r\n'
		}
		coal = true
	    }
	    else if (id_run_end == undefined){
		if (d.len > 1){
		    id_run_end = +mapping.filter(e=>+e[0]==d.start+d.len-1-1)[0][1]
		    txt= txt +(id_run_start+offset)+(d.len!=1?' '+(id_run_end+offset):'')+'\r\n'
		}
		coal = true
	    } else {
	    txt= txt +(id_run_start+offset)+(d.len!=1?' '+(id_run_end+offset):'')+'\r\n'
	    }
	}
    })
    return [txt, coal]
}

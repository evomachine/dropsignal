"""pipeline - Logic to apply the full analysis pipeline and CLI entry point.
This file is part of the dropsignal package.
Copyright 2016-2019 Guilhem Doulcier, Licence GNU AGPL3+
"""
import sys
import os
import shutil
import logging
import argparse
from glob import glob
from functools import partial

import pandas as pd
import numpy as np

from dropsignal import __version__
import dropsignal.protocol
import dropsignal.processing.binraw as binraw
import dropsignal.processing.quality as quality
import dropsignal.processing.rebuild as rebuild
import dropsignal.processing.aggregate as aggregate
import dropsignal.processing.export as export
import dropsignal.plugins

logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
logger.propagate = False


def pipeline(path, output_path=None, wipe_previous=False):
    """ Run the complete dropsignal pipeline on a folder.

    Args:
        path (str): path to the folder containing *raw files.
        output_path (str): where to store the output. Default to <path>/analysis
        wipe_previous (bool): If true, delete output_path before starting.
    """
    # Check input folder
    if not glob(os.path.join(path, '*.raw')):
        raise ValueError("No *.raw file found. Are you sure this is the right path ?")

    logger.info("="*30+" PIPELINE START "+"="*30)

    # Output folder configuration.
    if output_path is None:
        output_path = os.path.join(path, 'analysis')
    if wipe_previous:
        shutil.rmtree(output_path)
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    # Setting up the logger
    file_handler = logging.FileHandler(os.path.join(output_path, 'pipeline_log.txt'))
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s",
                                  "%m/%d/%Y %I:%M:%S")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.info("Using dropsignal v{}".format(__version__))
    try:

        # Loading protocol data
        logger.info("~~~ Loading protocol ~~~")
        ppath = {'protocol_path': os.path.join(path, 'protocol.json'),
                 'template_path': os.path.join(path, 'template.csv')}
        protocol, template = dropsignal.protocol.loader(**ppath)
        protocol["output_path"] = output_path
        export.metadata(protocol, output_path)

        # === Find all the files. === #
        logger.info("~~~ File discovery ~~~")

        records = binraw.discover(path)
        protocol["time_first"] = records['time'].min()
        protocol["duration"] = records['time'].max() - records['time'].min()
        protocol["time_last"] = records['time'].max()
        protocol["runs"] = records['run'].nunique()

        logger.info('{} runs discovered.'.format(protocol["runs"]))

        # === Peak detection and Integration === #
        logger.info("~~~ Signal Processing ~~~")

        process_param = {
            'window': protocol["window"],
            'window_size': protocol["window_size"],
            'threshold': protocol["detection_threshold"],
            'buffer_drops_max_number': protocol["buffer_drops_max_number"],
            'buffer_drops_waiting_time': protocol["buffer_drops_waiting_time"],
            'pmt': protocol['active_channels'],
            'channel_names': protocol['channels_names'],
            'output_path': output_path,
            'alignment_correction_channels': protocol['correct_alignment_with_detection'],
            'reference_ptd': protocol['reference_photodiode'],
            'advanced_params': protocol['advanced_detection_parameters'],
            'distance_ptd': protocol['distance_photodiode']}

        runs, peaks, protocol['T0_offset'] = binraw.process(records, **process_param)

        # Update protocol information: number of peaks
        if "expected_droplets" not in protocol:
            protocol["expected_droplets"] = int(np.ceil(runs.peaks.median()))
            logger.warning(("The train length (expected_droplet) was not defined "
                            "in the protocol. Falling back to the median number of "
                            "detected droplets: {}.").format(protocol["expected_droplets"]))
        else:
            protocol["expected_droplets"] = int(protocol["expected_droplets"])

        # === Run quality check === #
        logger.info("~~~ Run annotation and quality check ~~~")
        qc_tests = [partial(quality.qc_length, threshold=3),
                    partial(quality.qc_peak_nb, expected=protocol['expected_droplets']),
                    partial(quality.qc_coalescence, thres=protocol['max_coalescence'])]
        runs, bad_tags = quality.check_quality(qc_tests, runs,  protocol['bad_tags'])
        protocol['good_runs'] = runs.good.sum()
        protocol['bad_tags'] = bad_tags
        protocol['run_list'] = [int(x) for x in runs.id[runs.good].values]
        protocol['droplets_nb'] = runs[runs.good].peaks.max()
        logger.info(("{} runs annotated, {} are tagged as good."
                     "(Tag marked as bad: {})").format(protocol['runs'],
                                                       protocol['good_runs'],
                                                       protocol['bad_tags']))

        # === Reconstruct timeseries === #
        logger.info("~~~ Reconstruct the timeseries ~~~")

        # Rebuild the timeseries of each droplet according to the coalescence events.
        events, all_events = rebuild.get_events(runs, peaks,
                                                protocol['manual_coalescence_events'],
                                                protocol['automatic_coalescence_events'],
                                                protocol['use_automatic_coalescence_events'])
        droplet_dyn, runs, _, id_mappings, last_complete_info = rebuild.rebuild(runs, peaks,
                                                                                protocol['droplets_nb'],
                                                                                events,
                                                                                protocol['relative_times'])
        protocol['coalescence_event_list'] = all_events
        protocol['sorting_mapping_id_exp_to_id_run'] = list(
            id_mappings[last_complete_info]['exp_to_run'].items())
        protocol['last_complete_coalescence_information_run'] = last_complete_info
        logger.debug("Individual dynamics rebuilt accross {} runs".format(runs.good.sum()))

        # === Droplets annotation === #
        logger.info("~~~ Droplet annotation and quality check ~~~")
        droplets = quality.annotate_droplets(droplet_dyn)
        droplets["well"] = [protocol["droplet_to_well"][x] for x in droplets.index]
        if "well" in template and "description" in template:
            droplets = pd.merge(droplets, template.loc[:, ["well", "description"]],
                                left_on="well", right_on='well', how="left")
        droplets.rename(columns={"description": "group"}, inplace=True)
        if 'expected_growth' not in droplets.columns:
            droplets['expected_growth'] = 1
        logger.debug("{} droplets annotated.".format(droplets.shape[0]))

        # === Aggregate === #
        logger.info('~~~ Aggregating timeseries ~~~')
        aggregated = aggregate.aggregate_per_run(droplet_dyn, droplets)

        # === Apply plugins ===#
        data = {"dynamics": droplet_dyn, "droplets": droplets, "runs": runs}
        for name in protocol["enabled_plugins"]:
            logger.debug("~~~ Plugin: {}~~~".format(name))
            dropsignal.plugins.apply_plugin(name, data, protocol)

        # === Export === #
        logger.info("~~~ Export ~~~")
        protocol["status"] = "OK"
        export.analysis_folder(
            runs, droplets, droplet_dyn, aggregated, protocol, output_path)
        logger.info("~~~ END ~~~")
    finally:
        # Detach the handler it stops writing in the the file.
        logger.handlers.pop(logger.handlers.index(file_handler))
        file_handler.close()
    return path


def main():
    """ CLI entry point """
    if '--version' in sys.argv:
        print(__version__)
        sys.exit(0)

    description = ''' Extraction, conversion and visualization of experimental data from millifluidic droplet trains.

Most of the configuration is done using the protocol.json and
template.csv files. Use the `protocol` command to write example
file.'''

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog='''Available commands:
- pipeline: Process of the experiment in PATH.
- plugin: Execute the plugin given by --pname PLUGIN in PATH (must have run pipeline first).

Report bugs at https://gitlab.com/evomachine/dropsignal/issues.
You are using version {}
                                     '''.format(__version__))
    parser.add_argument('command',
                        choices=['pipeline', 'plugin'],
                        type=str,
                        help='Command. See below for a description.')
    parser.add_argument('path', metavar='PATH', type=str,
                        default='.', nargs="*",
                        help='Raw data location.')
    parser.add_argument('--pname', metavar="PLUGIN", type=str,
                        help=("Name of the plugin."
                              "Installed plugins: "
                              "{}").format(dropsignal.plugins.INSTALLED))
    parser.add_argument('--version', action='store_true',
                        help="Display version and quit.")

    args = parser.parse_args()

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
    logger.addHandler(console_handler)

    for path in args.path:
        logger.info('Path: {}'.format(path))
        if args.command == 'plugin':
            try:
                dropsignal.plugins.load_data_and_apply(path, args.pname)
            except Exception as ex:
                logger.exception("Error in plugin: {}".format(ex))
        elif args.command == 'pipeline':
            try:
                pipeline(path)
            except Exception as ex:
                logger.exception("Error in pipeline: {}".format(ex))

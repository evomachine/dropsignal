"""Poisson plugin for dropsignal.
This file is part of the dropsignal package.
Copyright 2016-2019 Guilhem Doulcier, Licence GNU AGPL3+

Will detect empty droplets, compute the expected number of droplets
that would have 1 cell and "2 cells or more" if the dilution was
following a Poisson Process. In order to study lag, it will also
compute the the distribution of times to reach a given threshold.
"""

import os
import shutil
import logging

import pandas as pd
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib
from matplotlib.ticker import FuncFormatter
import dropsignal.processing.quality as quality
import matplotlib.pyplot as plt


matplotlib.use('agg')

logger = logging.getLogger('dropsignal.plugins.poisson')

PARAMETERS = {"threshold": {"description": "Threshold value (0.1 to 5.0) used to discriminate drops. For each drop, the plugin determine the time for the measurement, defined in 'plugin.poisson.channel', to cross (rising) the threshold value. Time-to-threshold is obtained by linear interpolation on the two measurement points below and above the threshold value.  ", "default": 1.0},
              "channel": {"description": "Measurement used to detect `empty` (or `baseline`) droplets. You must use the channel names defined in  `channel_names` and provide a measurment kind in this format: 'CHANNELNAME_MEASUREMENT', valid measurement kind include 'max', 'median' or 'mean'. Example: 'fluo_2_max' ", "default": "fluo_2_max"},
              "plot_empty_drops": {"description": "If true, the plots that show, for each group, the droplets dynamics seggregation according to 'plugin.poisson.threshold' and 'plugin.poisson.max_time' will include the  'empty' (or 'baseline') droplets dynamics.", "default": True},
              "max_time": {"description": "Maximum time to qualify a non-'empty' droplets (in minutes). Droplets which have a defined time-to-threshold later than 'plugin.poisson.max_time' are tagged 'empty' (or 'baseline'), meaning the threshold crossing is considered to be too late.", "default": 600.0},
              "include_empty": {"description": "Includes `empty` (or `baseline`) droplets which have a defined time-to-threshold in the time-to-threshold distribution estimation and average calculation. These drops are tagged as `empty` (or `baseline`) because their time-to-threshold is later than 'plugin.poisson.max_time'. ", 'default': True},
              "estimate_density": {"description": "Perform a kernel density estimation of the time-to-threshold distribution estimation", 'default': False},
              "restrict_to_groups": {"description": "List the groups as defined in 'template.csv' to be analysed by the plugin, if empty all groups will be treated", "default": tuple()}}


def main(data, _, parameters):
    """Execute the plugin"""

    # Clear the export path.
    shutil.rmtree(parameters["export_path"])
    os.mkdir(parameters["export_path"])

    # convert to seconds.
    parameters['max_time'] *= 60
    parameters['max_time'] = min(data['dynamics']['time'].max(), parameters['max_time'])

    # Filter the data.
    data['droplets'].index.name = 'id_exp'
    if len(parameters['restrict_to_groups']):
        logger.debug('Analysing only groups: {}'.format(parameters['restrict_to_groups']))
        groups = frozenset(parameters['restrict_to_groups'])
        droplets = frozenset([row.id_exp
                              for k, row
                              in data['droplets'].reset_index().loc[:, ['id_exp', 'group']].iterrows()
                              if row.group in groups])
        data["droplets"] = data["droplets"].loc[droplets, :]
        data["dynamics"] = data["dynamics"][[x in droplets for x in data['dynamics'].id_exp]]

    # Measure empty droplets
    logger.debug('Measure time to reach threshold {} droplets on {} (tmax={})...'.format(
        parameters["threshold"], parameters["channel"], parameters["max_time"]))
    df = quality.time_to_reach_threshold(data["dynamics"],
                                         parameters["channel"],
                                         parameters["threshold"],
                                         parameters["max_time"])

    # Aggregate the number of empty droplets per groups.
    dr = data['droplets']
    merged = pd.merge(df, data['droplets'].reset_index().loc[:,
                                                             ['id_exp', 'group']], how='left')

    grouped = merged.groupby('group')
    group_info = pd.DataFrame({'drops': grouped.empty.count(),
                               'empty': grouped.empty.sum().astype(int)})

    if not parameters['include_empty']:
        grouped = merged[merged['empty'] == 0].groupby('group')
    else:
        grouped = merged.groupby('group')
    averagetime = grouped.time_to_reach_threshold.mean().to_dict()
    group_info['average_time'] = [averagetime[x]
                                  if x in averagetime else np.nan for x in group_info.index]

    # Plots !
    logger.debug('Plotting dynamics...')
    plot_dynamics(merged, data['dynamics'],
                  parameters['channel'],
                  parameters['export_path'],
                  parameters['threshold'],
                  parameters['max_time'],
                  not parameters['plot_empty_drops'])

    if parameters['estimate_density']:
        logger.debug('Kernel density estimations...')
        plot_kde(merged if parameters['include_empty'] else merged[merged['empty'] == 0],
                 parameters['export_path'])

    # Estimate Poisson parameters
    group_info['lambda'] = -np.log(group_info["empty"]/group_info["drops"])
    group_info['0_class'] = group_info['drops']*np.exp(-group_info['lambda'])
    group_info['1_class'] = group_info['drops'] * \
        np.exp(-group_info['lambda'])*group_info['lambda']
    group_info['2plus_class'] = group_info['drops'] * \
        (1-(1+group_info['lambda'])*np.exp(-group_info['lambda']))

    # Time format.
    group_info['average_time (h:m:s)'] = group_info['average_time'].apply(seconds_to_hms)
    merged['time_to_reach_threshold (h:m:s)'] = merged['time_to_reach_threshold'].apply(
        seconds_to_hms)
    group_info['average_time (minutes)'] = group_info['average_time'].apply(
        lambda x: x/60)
    merged['time_to_reach_threshold (minutes)'] = merged['time_to_reach_threshold'].apply(
        lambda x: x/60)

    merged.to_csv(os.path.join(parameters["export_path"], 'times.csv'))
    group_info.to_csv(os.path.join(parameters["export_path"], 'group_info.csv'))


def html_render(path, metadata, static_path=None):
    """ HTML render of the plugin results. Called by the web UI """
    if static_path is None:
        static_path = 'plugin_static/poisson/'

    # Load the data
    merged = pd.read_csv(os.path.join(path, 'analysis',
                                      'plugins',
                                      'poisson',
                                      'times.csv'))
    group_info = pd.read_csv(os.path.join(path,
                                          'analysis',
                                          'plugins',
                                          'poisson',
                                          'group_info.csv'))
    parameters = {k: metadata['plugin.poisson.'+k]
                  for k in PARAMETERS if 'plugin.poisson.'+k in metadata}

    html = """
    <h1>Poisson dilution analysis</h1>
    <p>Droplets are considered empty if the signal in {} stays below {}V before {}. </p>
    """.format(parameters["channel"],
               parameters["threshold"],
               tick_fmt(parameters["max_time"]*60, None))
    html += '<div class="plot"><img src="{}KDE.png"/></div>'.format(static_path)
    group_info = group_info.rename(columns={"average_time": "average_time (seconds)"})
    cat = {'0_class': 'model',
           '1_class': 'model',
           '2plus_class': 'model',
           'average_time (seconds)': 'measure',
           'average_time (h:m:s)': 'measure',
           'average_time (minutes)': 'measure',
           'drops': 'id',
           'average_time': 'measure',
           'empty': 'measure',
           'group': 'id',
           'lambda': 'model'}
    group_info.columns = pd.MultiIndex.from_tuples(
        [(cat[c], c) for c in group_info.columns])
    c_order = [('id', 'group'), ('id', 'drops'),  ('measure', 'empty'), ('measure', 'average_time (seconds)'),  ('measure', 'average_time (h:m:s)'),
               ('measure', 'average_time (minutes)'), ('model', 'lambda'), ('model', '0_class'),
               ('model', '1_class'), ('model', '2plus_class')]
    html += group_info[c_order].to_html(classes=['table', 'table-striped'])
    html += '<a href="{}group_info.csv">Download</a>'.format(static_path)
    for g in merged['group'].unique():
        html += '<div class="plot"><img src="{}{}.png"/><img src="{}{}_KDE.png"/></div>'.format(
            static_path, g, static_path, g)
    html += '<div style="max-height:300px;overflow:scroll;"> <h3> Raw data </h3><a href="{}times.csv">Download</a>'.format(
        static_path)
    html += merged.to_html(classes=['table', 'table-striped'])
    html += '</div>'
    return html


def seconds_to_hms(seconds):
    try:
        minutes = seconds//60
        return "{}:{}:{}".format(int(minutes//60), int(minutes % 60), int(seconds % 60))
    except ValueError:
        return '-'


def tick_fmt(d, _):
    h = d//60
    return "{}:{}:{}".format(int(h//60), int(h % 60), int(d % 60))


def plot_dynamics(merged, dynamics, axis, path, threshold, max_time, hide_empty=False):
    formatter = FuncFormatter(tick_fmt)
    if hide_empty:
        merged = merged[merged['empty'] == 0]
    for group, drops in merged.groupby('group'):
        fig = plt.figure(figsize=(15, 10))
        ax = plt.gca()
        ax.xaxis.set_major_formatter(formatter)

        drop_index = frozenset(drops.id_exp)
        color = {0: 'C1', 1: 'C0'}
        is_empty = merged.set_index('id_exp')['empty']
        for idx, v in dynamics[[x in drop_index for x in dynamics.id_exp]].groupby('id_exp'):
            ax.scatter(v["time"], v[axis], color=color[is_empty.loc[idx]], alpha=0.1)
            ax.plot(v["time"], v[axis], color=color[is_empty.loc[idx]])

        ylim = plt.ylim()
        xlim = plt.xlim()

        plt.scatter(drops['time_to_reach_threshold'],
                    [threshold]*len(drops),
                    marker="+",
                    color='r', zorder=100)

        plt.vlines(max_time, *ylim, alpha=.5)
        plt.hlines(threshold, *xlim, alpha=.5)

        plt.ylim(ylim)
        plt.xlim(xlim)

        plt.xlabel('Time (h:m:s)')
        plt.ylabel(axis+' (mv)')
        plt.title(group)
        plt.savefig(os.path.join(path, group+'.png'), bbox_inches='tight')
        plt.close(fig)


def plot_kde(merged, path):
    grid = np.linspace(merged.time_to_reach_threshold.min(),
                       merged.time_to_reach_threshold.max(), 200)
    formatter = FuncFormatter(tick_fmt)
    master_fig = plt.figure(figsize=(15, 10))
    master_ax = plt.gca()

    for group, data in merged.groupby('group'):
        x = data.time_to_reach_threshold.dropna().values
        fig = plt.figure(figsize=(15, 10))
        ax = plt.gca()

        if len(x) > 1 and np.std(x) != 0:
            kde = gaussian_kde(x)
            y = kde.evaluate(grid)

            # Plot on the master_plot
            line = master_ax.plot(grid, y, label=group)
            master_ax.fill_between(grid, y, color=line[0].get_color(), alpha=.1)

            # Plot on the individual plots
            ax.plot(grid, y, color=line[0].get_color())
            ax.fill_between(grid, y, color=line[0].get_color(), alpha=.5)

            ylim = plt.ylim()
            xlim = plt.xlim()
            ax.scatter(data.time_to_reach_threshold, [
                       0]*len(data), marker='|', color='k', alpha=.5)
            plt.ylim(ylim)
            plt.xlim(xlim)
        elif len(x) == 1:
            ax.scatter(data.time_to_reach_threshold, [
                       0]*len(data), marker='|', color='k', alpha=.5)
        ax.xaxis.set_major_formatter(formatter)
        ax.set_xlabel('Time (h:m:s)')
        ax.set_ylabel('Density')
        ax.set_title('Distribution of time to reach the threshold: {}'.format(group))
        plt.savefig(os.path.join(path, group+'_KDE.png'), bbox_inches='tight')
        plt.close(fig)
    plt.sca(master_ax)
    plt.xlabel('Time (h:m:s)')
    plt.ylabel('Density')
    plt.title('Distributions of time to reach the threshold')
    master_ax.xaxis.set_major_formatter(formatter)
    plt.legend()
    plt.savefig(os.path.join(path, 'KDE.png'), bbox_inches='tight')
    plt.close(master_fig)

""" protocol.spec - Protocol specifications
This file is part of the dropsignal package.
Copyright 2016 Guilhem Doulcier, Licence GNU AGPL3+

This file contains only constant. They are the hardcoded default for the protocols
and server options in dropsignal.

Each entry of the PROTOCOL_SPEC contains at least:
- (a key)
- default: default value for this option.
- category: will be used to sort the options in the editor.
- description: will be displayed in the protocol editor.
Additionally:
- valid_values: list of valid values.
"""

SERVER_OPTIONS_SPEC = {
    "web_initial_port": 8080,
    "web_plugin_list": ['train_heatmap', 'poisson'],
    "web_browser": False,
    "web_firefox_path": "C:\\Program F~\\Mozilla Firefox\\firefox.exe",
    "web_default_path": ".",
    "web_public": False,
}

PROTOCOL_SPEC = {
    'enabled_plugins': {
        'description': "List of plugins that will be run at the end of each pipeline execution. Plugins enable additional specific data processing or graphs creation. Currently [”poisson”] : sort curves based on signal threshold and compute statistics related to Poisson distribution for limiting dilution experiments.",
        'basic': False, "category": 'Plugins',
        'default': []
    },
    'title': {
        'description': "User may log any useful information for himself or colleagues. It will be saved in the protocol file. It is not used by the program.", "category": 'Metadata',
        'basic': False, 'default': None
    },
    'droplets_per_well': {
        'default': 10, 'basic': False,
        'description': 'Number of droplets per well, ignored if the template file contains a `droplet_number` column',
        "category": 'Template'
    },
    'relative_times': {
        'category': 'Detection', 'default': False, 'basic': True,
        'description': 'if True, the times relative to the droplet creation will be used for display, otherwise the same absolute time (starting at experiment start time) is used for all drops. Both `absolute_time` and `relative_time` are available in the data output `droplet_dynamics.csv`.'
    },
    'visit_mode': {
        'default': 'sequential', 'basic': True,
        'valid_values': ('sequential', 'hiccup'),
        'description': ('Way the wells are visited by the robot at generation. `sequential`: the wells are visited sequentially generating all the droplets for one well before moving to the next well. `hiccup`: the wells are visited by non-overlapping pairs alterning a droplet from each one, until droplet_per_well droplets are done for each well before moving on to the next pair'),
        "category": 'Template'
    },
    'well_order': {
        'default': 'snake94',
        'description': 'Order in which the wells are visited by the sipper. It is ignored if the template file contains an `order` column',
        'basic': False,
        'category': 'Template'
    },
    'color_palette': {
        'default': ['#4e79a7', "#f28e2b", "#e15759", "#76b7b2", "#59a14f", "#edc948", "#b07aa1", '#ff9da7', "#9c755f", "#bab0ac"],
        'description': 'Colour palette used to assign colours to groups in `color_group`',
        'basic': False, 'category': 'Template'
    },
    'color_group': {
        'default': {'empty': 'C9', 'Empty': 'C9'},
        'description': 'Use this option to fix the color associated with a group (group names from template). Valid entries are hexadecimal color or CX where X is the index in the `color_palette` (starting from C0)',
        'basic': False, 'category': 'Template'
    },
    'window': {
        'description': 'Shape of the function used to filter the signal from the droplet detection channel. See https://docs.scipy.org/doc/scipy/reference/signal.html#window-functions',
        'basic': False, 'default': 'boxcar', "category": "Detection"
    },
    'window_size': {
        'description': 'Size (in measure points, example: 20) of the smoothing window used on the drops detection signal to smooth signal fluctuations that could cause false detections',
        'default': 10, "category": "Detection", 'basic': True,
    },
    'detection_threshold': {
        'description': 'General threshold for droplet detection, in (normalised units between 0 and 1 (for a desired threshold of 2.5 V, this value will be 5/2.5=0.5 ). It is used only if no other threshold is defined in `advanced_detection_parameters`',
        'default': .1, "category": "Detection", 'basic': True,
    },
    "active_channels": {
        'description': 'List of active photomultipliers (pmt) defined by the hardware configuration of your instrument. List among : ["pmt1", "pmt2", "pmt3", "pmt4", "pmt5"]',
        'default': ('pmt1', 'pmt2'), "category": "Detection",
        'basic': True,
    },
    "channels_names": {
        'description': 'User defined names of channels : change the name of each hardware acquisition `active_channels` ("pmt1", "pmt2", "pmt3", "pmt4", "pmt5") for a name describing the measurement. e.g. [["pmt1", "GFP"], ["pmt2", "Resazurin"], ["pmt3", "fluo_3"], ["pmt4", "fluo_4"], ["pmt5", "scattering"]]',
        'default': (('pmt1', 'fluo_1'), ('pmt2', 'fluo_2'), ('pmt3', 'fluo_3'), ('pmt4', 'fluo_4'), ('pmt5', 'scattering')),
        'basic': False, 'category': 'Detection'
    },
    "correct_alignment_with_detection": {
        'description': 'List of the hardware acquisition `active_channels` ("pmt#") in which each peak will be re-aligned with the detection signal by using signals convolution fucntion maximization',
        'basic': False, 'default': ('pmt1', 'pmt2', 'pmt3', 'pmt4'),
        'category': 'Detection'
    },
    "reference_photodiode": {
        'description': 'Define the master photodiode (`ptd#`) used as reference for the number of drops. Measurements of other photodiodes with a different number of drops will be dropped. Photodiodes are inherent to each optical module, therefore: “ptd1” accounts for “pmt1” and “pmt2”; “ptd2” for “pmt3” and “pmt4”, ptd5 for “pmt5”. “ptd3” and “ptd4” can’t be defined as “reference_photodiode”',
        'default': 'ptd1', 'category': 'Detection', 'basic': False,
    },
    "distance_photodiode": {
        'description': 'Distance between photodiode 1 and 2 in millimetres (used to compute droplet size and speed)',
        'default': 36, 'category': 'Detection', 'basic': False,
    },
    "manual_coalescence_events": {
        'description': 'This list of coalescence events which are confirmed as valid by the user will be used to build drops time series. The format is [[run_number, peak1_number, peak2_number], [run_number, peak1_number, peak2_number],...]. `run_number` corresponds to the last run before the coalescence. `peak_numbers` designate the numbers of the two peaks, which correspond to the two coalesced droplets, in the signal of the last run before coalescence. Peak number can be verified with the `Signal` display of dropsignal.',
        'default': [], "category": "Droplet coalescence", 'basic': False
    },
    "automatic_coalescence_events": {
        'description': 'Use a simple heuristic to suggest coalescence events. The suggested coalescence event can be reviewed in the `Signal` display of dropsignal. Please verify the validity of these suggested coalescence events and report them in the list "manual_coalescence_events" for the corresponding corrections drop time series to be applied.',
        'default': False, "category": "Droplet coalescence",
        'basic': False
    },
    "use_automatic_coalescence_events": {
        'description': '(EXPERIMENTAL) Take into account the automatic coalescence events suggestions if the run is not included in the "manual_coalescence_events" list.',
        'default': False, "category": "Droplet coalescence",
        'basic': False
    },
    "max_coalescence": {
        'description': 'Maximum number of coalesced droplets between two run under which automatic coalescence detection is performed',
        'default': 2, "category": "Droplet coalescence", 'basic': False,
    },
    "export_xls": {
        'description': 'Create a consolidated results.xlsx file in "/analysis" folder',
        'default': False, "category": "Export", 'basic': False,
    },
    "individual_csv": {
        'description': 'Export individual droplet dynamics in the droplet folder',
        'default': True,
        "category": "Export",
        'basic': False
    },
    "advanced_detection_parameters": {
        'description': 'Change detection parameters for specific photodiode (ptd). Format: `{"ptdX":{"PARAMETER":VALUE, ...}, ...}`. Example:{"ptd1":{"threshold":0.3, "window_size":20}, "ptd5":{"threshold":0.5}}. Authorised keys are: "threshold","window","window_size". This option overrule the general parameters.',
        'default': {}, 'category': 'Detection', 'basic': False
    },
    "buffer_drops_max_number": {
        'description': 'Maximum number of buffer drops to remove.',
        'default': 15, 'category': 'Detection', 'basic': False
    },
    "buffer_drops_waiting_time": {
        'description': 'Minimal time gap between buffer drops and the actual train (in seconds)',
        'default': 40.0, 'category': 'Detection', 'basic': False
    },
    "bad_tags": {
        'category': 'quality_control',
        'description': 'List of tags that would result in ignoring the run for the rest of the analysis.',
        'basic': False,
        'default': ("partial_train", "too_many_droplets",
                    "dramatic_coalescence", "probable_misscount",
                    "too_short")
    }
}

"""protocol.mappings ~ various function to parse protocol and templates
This file is part of the dropsignal package.
dropsignal - Guilhem Doulcier 2018 AGPL3+
"""

import logging
from collections import defaultdict
logger = logging.getLogger('dropsignal.protocol')
WELL_ORDER = {}

def snake_order(rows, columns, back_to_init=True):
    """Return an ordering of the wells "in a snake shape" (to the end of a
    row then starting the next row from the end). Well names are the
    concatenation of a row and column name.

    Args:
        rows (iter): Rows names.
        columns (iter): Colums names.
        back_to_init (bool): If true go back to the first well at the end.
    """
    order = []
    for i, row in enumerate(rows):
        if i%2 == 0:
            direction = 1
        else:
            direction = -1
        for col in columns[::direction]:
            order.append("{}{}".format(row, col))
    if back_to_init:
        order.append(order[0])
    return tuple(order)

WELL_ORDER["snake96"] = snake_order('ABCDEFGH', range(1, 13))
WELL_ORDER["snake96_noinit"] = snake_order('ABCDEFGH', range(1, 13), back_to_init=False)
WELL_ORDER["snake90"] = snake_order('ABCDEFGH', range(1, 13), back_to_init=False)[3:-3]
WELL_ORDER["snake94"] = snake_order('ABCDEFGH', range(1, 13), back_to_init=False)[1:-1]

def update_protocol_from_template(protocol, template):
    """ Update the information in protocol with the info from template.

    Add the following entries to the protocol:
    - 'well_order'
    - 'expected_droplets'
    - 'droplet_per_well'
    - 'droplet_to_well'
    - 'well_to_group'
    - 'color_to_group'
    """

    # Get the order in which the wells are visited
    if 'order' in template.columns:
        order = template.sort_values('order').well
        if 'well_order' in protocol:
            logger.debug("`well_order` found in template, ignoring `well_order` value in protocol")
            del protocol['well_order']
    else:
        if protocol['well_order'] not in WELL_ORDER:
            logger.error("Unknown well order: {}".format(protocol['well_order']))
            raise ValueError("Unknown well order: {}".format(protocol['well_order']))
        order = WELL_ORDER[protocol['well_order']]

    # Get the number of droplets in each well
    if 'droplet_number' in template.columns:
        droplets_per_well = template.set_index('well').ix[order, 'droplet_number'].values
        protocol["expected_droplets"] = template.droplet_number.sum()
        if 'droplets_per_well' in protocol:
            logger.debug(("`droplet_number` found in template, ignoring `droplet_per_well` "
                          "value in protocol"))
            del protocol['droplets_per_well']
    else:
        droplets_per_well = protocol['droplets_per_well']

    # Build the droplet<->well mappings
    protocol["droplet_to_well"], protocol["well_to_droplets"] = droplet_mapping(droplets_per_well,
                                                                                order,
                                                                            protocol['visit_mode'])
    protocol["expected_droplets"] = len(protocol["droplet_to_well"])
    logger.info("Expecting a {} droplets train".format(protocol['expected_droplets']))

    # Add groups, tags and dillutions.
    if len(template):
        protocol["droplets"] = template_to_droplets(template, protocol['well_to_droplets'])
    if "droplets" not in protocol:
        protocol["droplets"] = []

    # Group colors (and well labels)
    protocol['well_to_group'] = {}
    color_usage = {x:0 for x in protocol['color_palette']}
    for k, v in protocol['color_group'].items():
        if v[0] == 'C':
            protocol['color_group'][k] = protocol['color_palette'][int(v[1:])]
        elif v[0] != '#':
            less_used_color = sorted(color_usage.items(), key=lambda x: (x[1], x[0]))[0][0]
            protocol['color_group'][k] = less_used_color
        color_usage[protocol['color_group'][k]] += 1
    for group in sorted(protocol['droplets'], key=lambda x: x['label']):
        for well in group['well']:
            protocol['well_to_group'][well] = group['label']
        if group['label'] not in protocol['color_group']:
            less_used_color = sorted(color_usage.items(), key=lambda x: (x[1], x[0]))[0][0]
            protocol['color_group'][group['label']] = less_used_color
            color_usage[less_used_color] += 1
    return protocol

def droplet_mapping(droplet_per_well, order, visit_mode):
    """Return two dictionnaries that maps droplets_id->well and well->droplet_id.

    Args:
        n (int or iterable): number of droplets made by well.
        order (iterable): order in which the wells are visited.
        visit_mode (str): 'sequential' or 'hiccup'. cf visit mode in the protocol file.
    """
    droplet_to_well = defaultdict(lambda: '00')
    well_to_droplet = defaultdict(list)

    possible_visit_modes = ('sequential', 'hiccup')
    if visit_mode not in possible_visit_modes:
        raise ValueError('Unknown visit mode: {} (should be one of: {})'.format(visit_mode,
                                                                                possible_visit_modes))

    try:
        droplet_per_well[1]
    except TypeError:
        # droplet_per_well is not iterable
        for i, well in enumerate(order):
            if visit_mode == 'sequential':
                drops_id = list(range((i*droplet_per_well),
                                      (i+1)*droplet_per_well))
            elif visit_mode == 'hiccup':
                drops_id = list(range((i-i%2)*droplet_per_well+i%2,
                                      (i-i%2)*droplet_per_well+i%2+2*droplet_per_well,
                                      2))
            well_to_droplet[well] += drops_id
            for j in drops_id:
                droplet_to_well[j] = well
    else:
        # droplet_per_well is iterable
        current = 0
        if visit_mode == 'sequential':
            for _, (well, nb) in enumerate(zip(order, droplet_per_well)):
                drops_id = list(range(current, current+nb))
                current += nb
                well_to_droplet[well] += drops_id
                for i in drops_id:
                    droplet_to_well[i] = well
        elif visit_mode == 'hiccup':
            ## Wells are visited by pairs.
            ## All the drops to be made in a pair
            ## are exhausted before going to the next pair.
            for (well1, ndrops1), (well2, ndrops2) in zip(zip(order[::2], droplet_per_well[::2]),
                                                          zip(order[1::2], droplet_per_well[1::2])):
                if ndrops1 != ndrops2:
                    logger.warning(('Well pair: ({}, {}) has two different number'
                                    'of drops ({}, {}) while in hiccup mode.'
                                    '').format(well1, well2, ndrops1, ndrops2))
                    warning = []
                    drop0 = current
                to_add = [ndrops1, ndrops2]
                for i in range(ndrops1+ndrops2):
                    if (i%2 == 0 and to_add[0]) or (i%2 == 1 and not to_add[1]):
                        well_to_droplet[well1] += [current]
                        droplet_to_well[current] = well1
                        to_add[0] -= 1
                        if ndrops1 != ndrops2:
                            warning.append(well1)
                    elif (i%2 == 1 and to_add[1]) or (i%2 == 0 and not to_add[0]):
                        well_to_droplet[well2] += [current]
                        droplet_to_well[current] = well2
                        to_add[1] -= 1
                        if ndrops1 != ndrops2:
                            warning.append(well2)
                    current += 1
                if ndrops1 != ndrops2:
                    logger.warning(('The actual order between '
                                    'drop {} and {} will be: {}.'
                                    '').format(drop0, current, ' '.join(warning)))
    return droplet_to_well, well_to_droplet

def template_to_droplets(template, rule, default=None):
    """ Get a list of droplet groups from the template

    Args:
        template (pd.Dataframe): required collums are Description, Well and Dillution,
        default (str): label of the default id.
        well_to_drop (dict): indices of the droplets when given a well.
    """
    def empty():
        """An empty droplet group"""
        return {'id':[], 'expected_growth': False, 'well': [], 'dilution': 1}

    droplets = defaultdict(empty)
    for _, df in template.reset_index().iterrows():
        if "dilution" in template.columns and template.dilution.nunique() > 1:
            label = "{} [{:1.1e}]".format(df.description, df.dilution)
            droplets[label]['dilution'] = df.dilution
        else:
            label = df.description
        droplets[label]['label'] = label
        droplets[label]['well'].append(df.well)
        if "expected_growth" in template.columns:
            droplets[label]['expected_growth'] = bool(df.expected_growth)
        droplets[label]['id'].append(rule[df.well])
    if default is not None:
        droplets[default]['id'] = 'default'
    return list(droplets.values())

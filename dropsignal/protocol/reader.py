""" reader.io - Reading template and protocol files, perform correctness check.
This file is part of the dropsignal package.
Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
"""
import logging
import json
import os
import pandas as pd
logger = logging.getLogger('protocol')


def protocol(protocol_path: str):
    """ Load the protocol from json file in protocol_path
    Perform simple correctness check. """
    out = {}
    logger.info("Reading protocol in {}".format(protocol_path))

    if not os.path.exists(protocol_path):
        raise FileNotFoundError('Protocol file not found: {}'.format(protocol_path))

    # Load the protocol.json
    try:
        # Remove empty lines and comments.
        with open(protocol_path, 'r') as protocol_file:
            text = [x.strip()
                    for x in protocol_file.readlines()
                    if not x.startswith("#")
                    and len(x.strip())]
        try:
            out.update(json.loads('{\n'+',\n'.join(text)+'\n}'))
        except ValueError as ex:
            raise ValueError('Incorrect syntax: {}'.format(ex))
    except Exception as ex:
        raise ValueError(('Failed to load experimental protocol: '
                          '{}, {}').format(protocol_path, ex))

    # Check input correctness
    if 'manual_coalescence_events' in out and out['manual_coalescence_events']:
        try:
            for event in out['manual_coalescence_events']:
                assert event[0] == int(event[0])
                try:
                    assert event[1] == int(event[1])
                except TypeError:
                    for i in event[1]:
                        assert i == int(i)
        except (IndexError, TypeError, AssertionError):
            logger.error(('Option manual_coalescence_events is badly formated'
                          'shoud be a list of `event` [event, event...] with the'
                          'format event=[run,drop] or event=[run, [drop,drop...]]'
                          '(`run` and `drop` are integers)'))
            raise ValueError('`manual_coalescence_events` is badly formated.')
    return out


def template(template_path: str):
    """ Load the template from csv file in template_path
    Perform simple correctness check. """
    if not os.path.exists(template_path):
        raise ValueError('Template file not found: {}'.format(template_path))

    try:
        logger.info("Reading template in {}".format(template_path))
        out = pd.read_csv(template_path, comment='#').fillna(1)
        out.columns = [str.lower(str(x)) for x in out.columns]
        required_columns = ["description", 'well']

        # Check for missing columns
        if any([x not in out.columns for x in required_columns]):
            raise ValueError(
                "Missing one of the required columns: {}".format(required_columns))

        # Check for bad order column.
        if 'order' in out.columns:
            if any(pd.isna(out['order'])):
                raise ValueError('Order must be specified for all row')

            try:
                out['order'] = out['order'].apply(pd.to_numeric)
            except ValueError as ex:
                raise ValueError('Invalid order column: {0}'.format(ex))

            if out.order.nunique() != len(out.order):
                raise ValueError('Order must be unique for all row')
    except Exception as ex:
        raise ValueError('Failed to load template: {}'.format(ex))
    return out

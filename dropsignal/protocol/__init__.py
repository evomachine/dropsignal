"""protocol - Protocol metadata manipulation
This file is part of the dropsignal package.
Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+

This module control all the parsing of protocol and config files.

In general the protocol values used during an execution of the
pipeline come from (in increasing order of priority):

- the specifications default PROTOCOL_SPEC (protocol.spec)
- the local machine default (in LOCAL_DEFAULT_PATH)
- the current experiment protocol ([EXPERIMENT]/protocol.json)

This module has the main protocol loader and functions to get the default values.

Submodules are:

protocol.reader: read the file and perform correctness check.
protocol.spec: protocol specification (constants).
protocol.mappings: convenience functions for protocol parsing.
"""
import logging
import platform
import time
import os
import json

import pandas as pd

from dropsignal import __version__, APP_PATH
from dropsignal.protocol import reader
from dropsignal.protocol import mappings
from dropsignal.protocol.spec import PROTOCOL_SPEC, SERVER_OPTIONS_SPEC
import dropsignal.plugins

logger = logging.getLogger('dropsignal')

### LOAD LOCAL PROTOCOL ###
LOCAL_DEFAULT_PATH = os.path.join(os.path.join(APP_PATH, 'local_protocol.json'))
if os.path.exists(LOCAL_DEFAULT_PATH):
    try:
        LOCAL_PROTOCOL = reader.protocol(LOCAL_DEFAULT_PATH)
    except Exception as ex:
        logger.error('Failed to read local protocol {} : {}'.format(
            LOCAL_DEFAULT_PATH, ex))
        raise ex
    logger.info(("Local protocol ({} entries) "
                 "successfully loaded from {}"
                 "").format(len(LOCAL_PROTOCOL), LOCAL_DEFAULT_PATH))
else:
    logger.info('No local protocol found in {}'.format(LOCAL_DEFAULT_PATH))
    LOCAL_PROTOCOL = {}

PLUGINS_SPEC = dropsignal.plugins.config_dict()
############################


def loader(protocol_path: str, template_path: str = None):
    """Build the protocol dict by loading data fron the protocol.json and
    template.csv files. See the documentation for the syntax of those
    files.
    """
    protocol = default_protocol()

    # Read the protocol file
    try:
        protocol_from_file = reader.protocol(protocol_path)
    except FileNotFoundError as ex:
        logger.error(ex)
        protocol_from_file = {}
    # Data from the file
    protocol.update(protocol_from_file)

    # Add pipeline info
    protocol["pipeline_infos"] = {'host': "|".join(platform.uname()),
                                  "timestart": time.asctime(),
                                  "version": __version__}
    protocol["status"] = "Running"

    # Read the template file
    if template_path is not None and os.path.exists(template_path):
        template = reader.template(template_path)
    else:
        template = default_template()

    # Update the protocol from the template.
    protocol = mappings.update_protocol_from_template(protocol, template)

    return protocol, template


def compare_with_default(protocol, default=None):
    """Check wich keys from the protocol are modified from the default value.
    Return a dict key->true if has been modified from the default."""
    if default is None:
        default = default_protocol()
    modified = {}
    for key, value in default.items():
        modified[key] = protocol[key] != json.dumps(value) if (key in protocol) else False
    return modified

# GET DEFAULT:


def default_protocol(parent_protocol=None):
    """ Get default protocol values, i.e. spec potentially updated by LOCAL_PROTOCOL"""
    spec = get_spec()
    protocol = {k: v['default'] for k, v in spec.items() if 'default' in v}
    local_protocol = {k: v for k, v in LOCAL_PROTOCOL.items() if k in protocol}
    protocol.update(local_protocol)
    if parent_protocol is not None:
        protocol.update(parent_protocol)
    return protocol


def default_template():
    """ Return the default template table (as a dataframe)"""
    order = mappings.WELL_ORDER[default_protocol()['well_order']]
    df = pd.DataFrame({"well": order,
                       "order": range(len(order)),
                       "droplet_number": [10]*len(order),
                       "description": ["Empty"]*len(order)})
    df["well_sort"] = df.well.apply(lambda x: (x[0], int(x[1:])))
    df.sort_values('well_sort', inplace=True)
    return df.loc[:, ["well", "description",
                      "order", "droplet_number"]]


def get_spec():
    """ Return the protocol specifications with plugins included """
    spec = PROTOCOL_SPEC
    spec.update(PLUGINS_SPEC)
    return spec


def web_options():
    """Return the Webserver options, potentially modified by the local
    protocol"""
    config = {k: v if k not in LOCAL_PROTOCOL
              else LOCAL_PROTOCOL[k]
              for k, v in SERVER_OPTIONS_SPEC.items()}
    return config

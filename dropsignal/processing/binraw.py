"""processing_binraw.py -- Import data from the instrument's binary format
This file is part of the dropsignal package.
Copyright 2016-2019 Guilhem Doulcier, Licence GNU AGPL3+
"""
import logging
from glob import glob
import os
import pickle
from hashlib import sha1
import json

import pandas as pd
import numpy as np

import dropsignal.processing.signal as processing

logger = logging.getLogger('dropsignal.processing')
logger.setLevel(logging.DEBUG)

# Default channels names.
CHANNEL_NAMES = {'ptd1': 'detection_1',
                 'ptd2': 'detection_2',
                 'pmt1': 'fluo_1',
                 'pmt2': 'fluo_2',
                 'pmt3': 'fluo_3',
                 'pmt4': 'fluo_4',
                 'ptd5': 'detection_scattering',
                 'pmt5': 'light_scattering',
                 'ptd3': 'detection_camera',
                 'ptd4': 'detection_head'}

# This is the mapping between photo-diodes and photo-multipliers.
# (i.e. on which photo-diode we should detect the drops to measure on
# the associated photo-multiplier)
PTD2PMT = {'ptd1': ['pmt1', 'pmt2'],
           'ptd2': ['pmt3', 'pmt4'],
           'ptd3': [], 'ptd4': [], 'ptd5': ['pmt5']}
PMT2PTD = {'pmt1': 'ptd1', 'pmt2': 'ptd1', 'pmt3': 'ptd2', 'pmt4': 'ptd2', 'pmt5': 'ptd5'}

# The format of the binary data:
BINRAW_FORMAT = np.dtype([('time', '<i4'), ]+[(x, '<i2') for x in ['pmt1', 'pmt2', 'pmt3',
                                                                   'pmt4', 'pmt5', 'ptd1',
                                                                   'ptd2', 'ptd3', 'ptd4',
                                                                   'ptd5', 'temperature']])

# How many drops are framing the train (on both side) when the train
# is detected for the first time in the sipper head.
PADDING_DROPS = 10


def discover(path):
    """Find the binary raw data file in path"""
    def f(x):
        try:
            run = int(os.path.basename(x).split('.')[0][3:])
        except ValueError:
            logger.warning('Failed to parse the name of raw file {}'.format(x))
            run = None
        return {'path': x, 'run': run}
    records = [f(x) for x in glob(os.path.join(path, "*.raw"))]
    records = pd.DataFrame([x for x in records if x['run'] is not None])
    records.sort_values('run', inplace=True)
    records.reset_index(inplace=True, drop=True)
    records['direction'] = [['Forth', 'Back'][i % 2] for i in range(len(records))]
    records['time'] = np.arange(len(records))
    return records


def extract(file):
    """Extract the binary file and return a pandas DataFrame.

    Args:
        file (str): A binary file with records of 26 bytes. Little endian.
            0-4: Time (in ms)
            4-14: Photomultipliers (in mV)
            14-24: Photodiodes (in mV)
            24-26: temperature (in 1/100C)
    """

    data = pd.DataFrame(np.fromfile(file, dtype=BINRAW_FORMAT))

    # Convert time to seconds:
    data['time'] /= 1000

    # Convert measures to volt:
    data.loc[:, ['pmt1', 'pmt2', 'pmt3', 'pmt4', 'pmt5',
                 'ptd1', 'ptd2', 'ptd3', 'ptd4', 'ptd5']] /= 1000

    # Convert temperatures to degrees:
    data['temperature'] /= 100
    return data


def process(records, window, window_size, threshold, buffer_drops_max_number, buffer_drops_waiting_time, pmt, channel_names,
            output_path='.',
            ptd=('ptd1', 'ptd2', 'ptd3', 'ptd4', 'ptd5'),
            reference_ptd='ptd1',
            advanced_params=None,
            alignment_correction_channels=('pmt1', 'pmt2', 'pmt3', 'pmt4'),
            distance_ptd=36):
    """Process all the signal files within records, detecting the peaks and measuring their size.

    Args:
        records (pd.DataFrame): required columns: run, path, direction, tags, duration, time_start.
        window (str): smoothing window to use when detecting drops.
        window_size (int): width of the smoothing window in number of measure points
    Returns:
        runs (pd.DataFrame): With columns id, peaks, duration, direction.
        peaks (dict): Double dictionary peaks[RUN_ID][CHANNEL_NAME] is a pd.DataFrame
    """
    advanced_params = None if advanced_params is None else advanced_params
    runs = []
    peaks = {}
    cnames = CHANNEL_NAMES.copy()
    cnames.update(channel_names)

    T0 = None
    logger.info('Reading Raw data: PTD: {}, PMT: {}'.format(ptd, pmt))

    for _, record in records.iterrows():
        logger.info('--- Run {} ---'.format(record['run']))

        # Cache prep.
        failed = False
        advanced = json.dumps(advanced_params, sort_keys=True)
        parameters_hash = sha1(str((window, window_size, threshold, ptd, pmt, T0,
                                    advanced, buffer_drops_max_number, buffer_drops_waiting_time,
                                    list(cnames.keys()).sort())).encode('utf-8')).hexdigest()
        cache_dir = os.path.join(os.path.dirname(record['path']), 'cache')
        if not os.path.exists(cache_dir):
            os.mkdir(cache_dir)
        record_pkle = os.path.join(cache_dir, os.path.basename(
            record['path']).replace('.raw', '_{}.pkle'.format(parameters_hash)))

        if os.path.exists(record_pkle):
            try:
                with open(record_pkle, 'rb') as file:
                    run_row, peaks[record['run']], T0 = pickle.load(file)
                    logger.debug(
                        'Loaded from pre-computed: {0[id]} ({0[direction]}): {0[peaks]} peaks - duration: {0[duration]}'.format(run_row))
            except Exception as ex:
                failed = True
                logger.error('Failed to load cached value {}'.format(record_pkle))

        if failed or not os.path.exists(record_pkle):
            # Extract RAW DATA
            signal = extract(record['path'])
            if T0 is None:
                T0 = np.min(signal['time'])
                logger.debug(
                    'Origin of absolute time set to the first record in this run ({})'.format(T0))
            signal['time'] -= T0

            # "Back" runs should be inverted.
            direction = 1 if record['direction'] == 'Forth' else -1

            if record['run'] == 0:
                # Run 0 is treated differently, we only look at the
                # head detection signal and store only the time of
                # each droplet (its creation time) We remove 10
                # droplet before and after: these padding droplet are
                # a constant of the machine.
                logger.info('Run 0 ~ Looking for droplet creation times.')
                detect_param = {'window': window,
                                'window_size': window_size, 'threshold': threshold}
                if 'ptd4' in advanced_params:
                    logger.debug("Using advanced parameters: {}".format(
                        advanced_params['ptd4']))
                    detect_param.update(advanced_params['ptd4'])
                (peak_centers, peak_inter,
                 detection, starts, ends) = processing.detect_peaks(signal['ptd4'],
                                                                    **detect_param)
                (peak_centers, peak_inter,
                 detection, starts, ends) = processing.remove_buffer_drops(peak_centers, peak_inter,
                                                                           detection, starts,
                                                                           ends, signal['time'],
                                                                           buffer_drops_max_number,
                                                                           buffer_drops_waiting_time,)

                droplet_creation = peak_centers
                measure_points = ends - starts
                droplet_creation_time = signal['time'][droplet_creation].reset_index(
                    drop=True)

                logger.info('{} droplet creation times found'.format(
                    droplet_creation_time.shape[0]))
                peaks[record['run']] = {'__measure__':
                                        pd.DataFrame({'time': droplet_creation_time,
                                                      'id_run': np.arange(droplet_creation_time.shape[0]),
                                                      'measure_points': measure_points})}

            else:
                # Mesure peaks
                peaks[record['run']] = measure_run(signal, window, window_size,
                                                   threshold,
                                                   buffer_drops_max_number, buffer_drops_waiting_time,
                                                   ptd, pmt, cnames,
                                                   advanced_params,
                                                   direction, alignment_correction_channels,
                                                   reference_ptd, distance_ptd)

            run_row = {'peaks': peaks[record['run']]['__measure__'].shape[0] if '__measure__' in peaks[record['run']] else 0,
                       'id': record['run'],
                       'tags': list(),
                       'direction': direction,
                       'duration': signal['time'].max() - signal['time'].min()}
            with open(record_pkle, 'wb') as file:
                pickle.dump((run_row, peaks[record['run']], T0), file)

        # Add to the run list.
        if run_row['peaks']:
            runs.append(run_row)
        else:
            del peaks[run_row['id']]
    processing.concat_peaks(peaks).to_csv(
        os.path.join(output_path, 'signal_annotation.csv'))
    runs = pd.DataFrame(runs)
    runs["time_start"] = np.concatenate(([0.], runs.duration.cumsum()[:-1]))

    return runs, peaks, T0


def measure_run(signal, window, window_size, threshold, buffer_drops_max_number, buffer_drops_waiting_time,
                ptd, pmt, cnames, advanced_params=None, direction=1,
                alignment_correction_channels=(), reference_ptd='ptd1', distance_ptd=36):
    """Process a single run."""
    peaks = {}
    measures = {}
    advanced_params = None if advanced_params is None else advanced_params
    default_detect_param = {'window': window, 'window_size': window_size, 'threshold': threshold}
    # For all photodiodes...
    for p in ptd:
        detect_param = default_detect_param.copy()
        if p in advanced_params:
            logger.debug("Using advanced parameters: {}".format(advanced_params[p]))
            detect_param.update(advanced_params[p])
        # That have an associated pmt activated...
        if len(frozenset(pmt).intersection(frozenset(PTD2PMT[p]))):

            # Detect the peaks on the light deflection channel.
            (peak_centers, peak_inter,
             detection, starts, ends) = processing.detect_peaks(signal[p],
                                                                **detect_param)
            (peak_centers, peak_inter,
             detection, starts, ends) = processing.remove_buffer_drops(peak_centers,
                                                                       peak_inter, detection, starts, ends, signal[
                                                                           'time'],
                                                                       buffer_drops_max_number, buffer_drops_waiting_time)
            if len(peak_centers):
                measures[p] = processing.measure_droplet(
                    starts, ends, signal['time'])
                logger.info(
                    'Photo Diode: {} - Peaks: {}'.format(cnames[p], len(peak_centers)))

                for fluo in PTD2PMT[p]:
                    if fluo in pmt:
                        # Measure the fluorescence signal.
                        corr = fluo in alignment_correction_channels
                        peaks[cnames[fluo]] = processing.measure_peaks(
                            signal[fluo], detection, peak_inter, corr)
                        logger.debug('\t Measure associated PMT channel: {} - {} {}'.format(
                            fluo, cnames[fluo], '(with alignment correction)' if corr else '(without alignment correction)'))
        else:
            logger.debug(
                'Photo Diode: {} - Skipped (no associated PMT)'.format(cnames[p]))

    if reference_ptd not in measures:
        return {}

    if len(measures) > 1 and len(signal['time']) > 2:
        period = signal['time'][2] - signal['time'][1]
        peaks['__measure__'] = processing.reconciliate_ptd(
            measures, distance=distance_ptd, sampling_period=period, reference=reference_ptd).reset_index()
    else:
        peaks['__measure__'] = measures[reference_ptd].reset_index()

    # Flip the back and forth runs.
    for k in peaks.keys():
        peaks[k]['id_run'] = peaks[k]['id_run'][::direction].values
    return peaks

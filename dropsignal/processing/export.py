"""export - export functions
This file is part of the dropsignal package.
Copyright 2016-2019 Guilhem Doulcier, Licence GNU AGPL3+
"""
import os
import logging
import json
import pandas as pd
from dropsignal import __version__

logger = logging.getLogger('dropsignal.export')

def metadata(protocol, output_path):
    """Protocol export"""
    try:
        if protocol is not None:
            with open(os.path.join(output_path, 'metadata.json'), "w") as fi:
                json.dump(protocol, fi, skipkeys=True,
                          indent=True, sort_keys=True, default=str)
    except (TypeError, OSError) as ex:
        logger.error("Protocol export failed: {}".format(ex))
        logger.debug(protocol)

def analysis_folder(runs, droplets, droplet_dyn, aggregated,  protocol,  output_path):
    """ Populate the analysis folder """
    # Export Metadata
    try:
        metadata(protocol, output_path)
    except Exception as ex:
        logger.error("Metadata export failed")
        logger.error(ex)

    # Export dataframe as CSV and XLS.
    try:
        tables(runs, droplets, droplet_dyn,  aggregated, output_path, protocol['export_xls'],protocol['individual_csv'])
    except Exception as ex:
        logger.exception("Export of tables failed {}".format(ex))

def tables(runs, droplets, droplet_dyn, aggregated, output_path, export_xls=False, export_individual=True):
    """Export dataframe into csv files."""
    # CSV
    runs.fillna(0).to_csv(os.path.join(output_path, 'runs.csv'))
    def cfilter(x):
        suffix = x.split('_')[-1]
        return suffix != 'ptd1' and suffix != 'ptd2' and suffix != 'shift' and x != 'offset'
    droplet_dyn['droplet_id'] = droplet_dyn['id_exp'] + 1
    col = [x for x in droplet_dyn.columns if cfilter(x)]
    droplet_dyn.loc[:, col].sort_values(['id_exp','run']).set_index(['id_exp','run']).fillna(0).to_csv(os.path.join(output_path, 'droplet_dynamics.csv'))
    aggregated.set_index(['kind','key']).fillna(0).to_csv(os.path.join(output_path, 'aggregated_dynamics.csv'))
    droplets.index.name = "droplet"
    droplets.fillna(0).to_csv(os.path.join(output_path, 'droplet.csv'))

    # Export a folder with a single CSV file per droplet containing all measures.
    if export_individual:
        droplet_path = os.path.join(output_path, 'droplets')
        if not os.path.exists(droplet_path):
            os.mkdir(droplet_path)
        for drop, table in droplet_dyn.groupby("id_exp"):
            table.set_index('run').fillna(0).to_csv(os.path.join(droplet_path, '{:04}.csv'.format(drop)))

    # XLSx export
    if export_xls:
        logger.debug('results.xlsx generation...')
        with pd.ExcelWriter(os.path.join(output_path, 'results.xlsx')) as xl:
            droplets.to_excel(xl, 'droplets')
            runs.set_index('id').to_excel(xl, 'runs')

            droplet_dyn.pivot('run', 'id_exp', 'time').to_excel(xl, 'time')
            values = [x for x in droplet_dyn.columns
                      if '_' in x and x.split("_")[-1] in frozenset(('mean', 'max', 'value'))]
            for val in values:
                pivoted = droplet_dyn.pivot('run', 'id_exp', val)
                pivoted.to_excel(xl, val)
                pivoted.to_csv(os.path.join(output_path, val+".csv"))
    logger.debug('done.')

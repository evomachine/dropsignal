"""Rebuild -- rebuild droplet timeseries from run records.
This file is part of the dropsignal package.
Copyright 2016-2019 Guilhem Doulcier, Licence GNU AGPL3+
"""
import logging

from itertools import combinations
from collections import defaultdict

import numpy as np
import pandas as pd

logger = logging.getLogger('dropsignal')


def get_events(runs, peaks, manual_coalescence_events, automatic_coalescence_events, use_automatic_coalescence_events):
    """Get coalescence events

    Args:
         runs ()
         peaks ()
         manual_coalescence_events: (list) of run peak1, peak2.
         automatic_coalescence_events: (bool) if true try to find suggested coalescence events.
         use_automatic_coalescence_events: (bool) if true add the suggested coalsecence events to the `events` output if no manual coalescence event was registered.

    Return: events, all_events. events is the final list of
    coalescence events. all_events contains all the events even if
    automatic and manual events are available for a single run.

    """
    # Convert coalescence events from the protocol.
    events_all = [(run, min(peak1, peak2)-1, 'user')
                  for run, peak1, peak2
                  in manual_coalescence_events]

    # Automatic detection of coalescence events.
    if automatic_coalescence_events:
        auto_events = get_coalescence_suggestions(runs, peaks)
        events_all += auto_events
        logger.info("Automatic coalescence events detection: {}".format([[run, drop+1, drop+2]
                                                                         for run, drop, kind in
                                                                         auto_events]))
    else:
        auto_events = None

    # Merging automatic and manual coalescence events.
    events = manual_coalescence_events[:]
    if auto_events and use_automatic_coalescence_events:
        manual_run = frozenset(r for r, _, _ in events)
        for r, drop, _ in auto_events:
            if r not in manual_run:
                events.append([r, drop, drop+1])
                logger.info(('Automatically detected coalescence events'
                             'for run {} added to the rebuild process.').format(r))
            else:
                logger.info(('Coalescence events for run {} are manually'
                             'inputed, ignoring automatically detected event'
                             'for the rebuild process.').format(r))
    return events, events_all


def rebuild(runs, peaks, n_drops, coalescence_events, relative_times: bool):
    """Rebuild the droplet timeseries from runs records."""

    # Read the coalescence informations
    jumps, id_mappings = coalescence_indices(runs.id, n_drops, coalescence_events)

    # Concatenate the dataframes
    droplet_dyn = droplet_dynamics(runs[runs.good].id, peaks, id_mappings)

    # Look in which runs we have complete coalescence information
    # for r, n_peaks in runs.set_index('id').peaks.iteritems():
    #    print('run', r, n_peaks, len(jumps[r]))
    runs['complete_coalescence_information'] = [n_drops-len(jumps[r]) == n_peaks
                                                for r, n_peaks
                                                in runs.set_index('id').peaks.iteritems()]

    # The sorting run is the last one for wich we have complete coalescence information.
    last_complete_info = runs[runs.complete_coalescence_information].id.max()

    logger.info(("{}/{} runs with complete coalescence information. Last one is {}"
                 "").format(runs.complete_coalescence_information.sum(), runs.good.sum(), last_complete_info))

    # Set the time columns
    if relative_times:
        logger.debug('Using relative times as default time')
        if droplet_dyn['relative_time'].isnull().any():
            logger.error(
                'Failed to compute relative time from droplet creation. Fix the detection parameters or set "relative_times" to `False` in the protocol.')
            raise ValueError('Failed to compute relative time from droplet creation.')
        droplet_dyn['time'] = droplet_dyn['relative_time']
    else:
        logger.debug('Using absolute times as default time')
    return droplet_dyn, runs, jumps, id_mappings, last_complete_info


def add_jumps(jumps, new_jumps):
    """Add new jumps to the list of jumps.

    Args:
        jumps (list): existing jumps
        new_jumps (list): jumps to add

    For each new jump we have to reduce by one the index of the existing
    jumps that occurs later in order to maintain consistency.
    """
    for new_jump in new_jumps:
        for j, jump in enumerate(jumps):
            if new_jump < jump:
                jumps[j] -= 1
        jumps.append(new_jump)
    return jumps


def mappings_id_exp_to_run(n_drops, jumps):
    """Build the mappings id_run <-> id_exp.

    Args:
        n_drops (int): number of droplets
        jumps (list): list of id_exp to jumps.
    """
    id_exp = np.arange(n_drops)
    id_run = np.arange(n_drops)
    for jump in jumps:
        id_exp[id_run > jump] += 1
    return {'exp_to_run': {e: r for e, r in zip(id_exp, id_run)},
            'run_to_exp': {r: e for e, r in zip(id_exp, id_run)}}


def coalescence_indices(runs, n_drops, manual_coalescence_events):
    """

    Coalescence events is a mapping {run: list_of_pairs}.
    Each pairs in the `list_of_pairs` are the peak_id (1-indexed)
    of the two droplets that coalesced during the run `run+1`
    """
    jumps = {runs.min()-1: []}
    coalescence_events = defaultdict(list)

    # Read the user input.
    # Convert the peak_id to a id_run
    for (run, id_peak1, id_peak2) in manual_coalescence_events:
        coalescence_events[run].append(min(id_peak1-1, id_peak2-1))

    for run in runs:
        new_jumps = coalescence_events[run]
        jumps[run] = add_jumps(jumps[run-1].copy(), new_jumps)
        logger.debug('Run {} - peak jumps are {}'.format(run, jumps[run]))

    id_mappings = {run: mappings_id_exp_to_run(n_drops, jumps[run]) for run in runs}
    return jumps, id_mappings


def concatenate_channels(peaks_run, columns=('mean', 'max', 'shift', 'std', 'cov', 'median', 'area')):
    """Concatenate a dictionnary of channels
           Args:
               peaks_runs (dict): Contains a dataframe by channel.
               columns (iterable): Columns specific to the channel.
           Returns: A pd.Dataframe with a <channel>_<col> column per <channel> and <columns>
               and using the others columns as index.
    """
    dyn = []
    if len(peaks_run) > 1:
        for channel, peak in peaks_run.items():
            if channel != '__measure__':
                dyn.append(peak.rename(columns=dict(
                    [(col, channel+"_"+col) for col in columns if col in peak.columns])))
                idx = [
                    x for x in dyn[-1].columns if "_".join(x.split("_")[:-1]) != channel]
                dyn[-1] = dyn[-1].set_index(idx)
        df = pd.concat(dyn, 1)
        df.reset_index(inplace=True)
        # Add the columns "time", "speed", "size" from the __measure__ channel.
        if '__measure__' in peaks_run.keys():
            df = pd.merge(df, peaks_run['__measure__'],
                          left_on='id_run', right_on='id_run')

        if "Time_value" in df.columns:
            df.rename(columns={"Time_value": "time"}, inplace=True)
        return df
    else:
        # Run 0 does not have the measures.
        return peaks_run['__measure__']


def droplet_dynamics(runs, peaks, id_mappings):
    """ Concatenate the runs in a single data frame

    Args:
        runs (iterable): Id of the runs to use
        peaks (iterable of dict): Each dictionnary correspond to a run,
            each one contains a dataframe by channel with columns: max, mean, value,
            center, id_run, time.
        id_mapping (iterable of dict): id_mapping[run]['run_to_exp'][id_run] maps id_run to id_exp.
    Returns:
        A single dataframe with:
           - center, id_run, time and run from the runs,
           - CHANNEL_max, CHANNEL_mean, CHANNEL_value from each channel
           - id_exp and offset according to the coalescence events.
    """
    droplet_dyn = []
    for run in runs:
        # We start by concatenating all channels.
        dyn = concatenate_channels(peaks[run])
        dyn['run'] = run

        # Apply index the mapping.
        dyn['id_exp'] = [id_mappings[run]['run_to_exp'][id_run] for id_run in dyn.id_run]
        dyn['offset'] = 0
        droplet_dyn.append(dyn)

    droplet_dyn = pd.concat(droplet_dyn).reset_index(drop=True)
    droplet_dyn = relative_time(droplet_dyn)
    return droplet_dyn


def relative_time(droplet_dyn, ref_run=0):
    """ Compute time relative to droplet creation """
    logger.info('Computing time relative to droplet creation (run {})'.format(ref_run))
    droplet_dyn['relative_time'] = np.nan
    droplet_dyn['absolute_time'] = droplet_dyn['time']
    try:
        for _, data in droplet_dyn.groupby('id_exp'):
            relative_times = data['time'] - data[data.run == ref_run].time.values[0]
            droplet_dyn.loc[relative_times.index, 'relative_time'] = relative_times.values
    except Exception:
        droplet_dyn['relative_time'] = np.nan
        logger.warning('Failed to compute times relative to droplet creation')
    return droplet_dyn


def get_coalescent_position_additive_size(current_size, previous_size):
    """Find the position of coalesced droplets by comparing two list of sizes.

    Args:
       current_size (iterable): size of each droplet at the current generation.
       previous_size (iterable): size of each droplet at the previous generation.

    Returns: (position, str) position of coalesced droplets and the
    reason why this coalescence event was selected.

    If droplet (n) and (n+1) coalesce this will return (n),
    If droplet (n) and (n+1) coalesce and (m, m+1) coalesce this will return (n,m),
    If droplet (n), (n+1), (n+2) coalesce this will return (n,n+1).

    This will brute force all combination of possible coalescnces and select the one that
    minimises the difference in droplet size, under the assumption that droplet size are
    strictly additive when two droplets coalesce.

    Note that `current_size` must be shorter than `previous_size`.

    """
    # Count the number of coalescnce
    nb_coal = len(previous_size)-len(current_size)

    # List all the possible coalescence events.
    possible_events = list(combinations(range(len(previous_size)-1), nb_coal))

    # Compute the size difference for each event.
    error = []
    for events in possible_events:
        size = defaultdict(lambda: 1)
        event = list(events)
        to_skip = []
        for i in event:
            start = i
            size[start] += 1
            to_skip.append(i+1)

            # If two subsequent are coalesced,
            # we must add all their size into a single peak in
            # order to keep the droplet sizes consistent.
            while i+1 in event:
                event.remove(i+1)
                to_skip.append(i+2)
                size[start] += 1
                i += 1
        new_size = [previous_size[n] if n not in size else np.sum(previous_size[n:n+size[n]])
                    for n in range(len(previous_size))
                    if n not in to_skip]
        error.append(sum(abs(x-y) for x, y in zip(new_size, current_size)))

    # Return the event with the smallest size difference
    pos = list(possible_events[np.argmin(error)])
    reason = 'Minimal droplet size total variation: {} | {}'.format(np.min(error), pos)
    return pos, reason


def find_coalescent_position(previous, current):
    """Locate coalescence events between two run by looking at droplet size.

    Args:
        previous (dict): data from previous run.
        current (dict): data from current run.

    Returns a list of indices. There will be one indice per
    coalescence event, i.e number of peak in current run minus the
    number of peak in the previous one.

    Inputs `previous` and `current` must contain a dataframe under the
    key "__measure__", whith one line per peak. Droplet size will be
    taken from the column `size`, or `measure_points` if `size` is
    missing. The only other required column is `id_run`.
    """

    column = 'size'
    if '__measure__' not in previous.keys() or '__measure__' not in current.keys():
        logger.warning(
            'Photodiode data missing. Impossible to find the position of the coalesced droplet.')
        return None

    if 'size' not in previous['__measure__'].columns or 'size' not in current['__measure__'].columns:
        logger.warning(
            'Droplet size data missing. Trying to use the number of measured points.')
        column = 'measure_points'

        if column not in previous['__measure__'].columns or column not in current['__measure__'].columns:
            logger.warning(
                'Number of measured points missing. Impossible to find the position of the coalesced droplet.')
            return None

    # Align the two runs as much as possible by matching id_runs.
    merged = pd.merge(previous['__measure__'].loc[:, ['id_run', column]],
                      current["__measure__"].loc[:, ['id_run', column]],
                      how='left', left_on='id_run', right_on='id_run',
                      suffixes=['_prev', '_curr'])

    # Find the coalescence event using size variation
    current_size = merged[column+'_curr'].dropna().values
    previous_size = merged[column+'_prev'].values
    pos, reason = get_coalescent_position_additive_size(current_size, previous_size)
    return pos, reason


def get_coalescence_suggestions(runs, peaks):
    coalescence_events = []
    coalescent_runs = [runs.id[k]
                       for k in runs.index
                       if 'coalescence_event' in frozenset(runs.tags[k])]
    for run in coalescent_runs:
        try:
            candidate, reason = find_coalescent_position(peaks[run-1], peaks[run])
        except Exception as ex:
            logger.exception(
                "Error in coalescence detection ({}) between run {} and {}".format(ex, run-1, run))
            candidate = None
        if candidate != None:
            for c in candidate:
                coalescence_events.append((run, c, 'auto'))
            logger.info('Between run {} and {} best candidate is drop {} ({}).'.format(run-1,
                                                                                       run,
                                                                                       candidate,
                                                                                       reason))
    logger.info('{} coalescence event(s) detected in {} suspicious runs.'.format(len(coalescence_events),
                                                                                 len(coalescent_runs)))
    return coalescence_events

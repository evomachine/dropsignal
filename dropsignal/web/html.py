"""html - HTML template for the web interface.
This file is part of the dropsignal package.
Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
"""
import os
import time
from dropsignal import __version__

def header(static_path, scripts, title, active,
           inline_script=None, plugins=None, links=None,
           lib=('d3v3', 'jquery', 'bootstrap'), loader=True):
    """Return the html header

    Args:
       static_path (str): relative path to static folder
       scripts (list): name of the jsfiles to add.
       inline_script (str): inline javascritp to inject.
       title (str): Page <title>
       active (str): Which `name` of the navbar highlight as active.
       links (list of tuple): Pairs (`name`, `path`) of links in the navbar.
    """
    lib = list(lib)

    static_path = os.path.join(static_path, '') # Add a trailing / if it does not exists already
    static_path = static_path.replace("\\", "/") # Oh windows why don't you use normal slashes...
    script_html = ''

    if loader:
        lib += ['anime']

    for script in scripts:
        script_html += ('<script type="text/javascript"'
                        ' src="{static}js/{script}.js"> </script>\n').format(static=static_path,
                                                                             script=script)
    if inline_script is not None:
        script_html += '<script>'+inline_script+'</script>'
    lib_import = {
        'anime':'''
        <script type="text/javascript" src="{static}lib/js/anime.min.js"></script>
        ''',
        'd3v4':'''
        <script type="text/javascript" src="{static}lib/js/d3.v4.min.js"></script>
        ''',
        'd3v3':'''
        <script type="text/javascript" src="{static}lib/js/d3.v3.min.js"></script>
        <script type="text/javascript" src="{static}lib/js/d3-queue.v2.min.js"></script>
        ''',
        'bootstrap':'''
        <script type="text/javascript" src="{static}lib/js/bootstrap.min.js"></script>
        <link href="{static}lib/css/bootstrap.min.css" rel="stylesheet">
        ''',
        'jquery':'<script src="{static}lib/js/jquery.min.js"></script>'
    }
    lib_html = ''
    for k in lib:
        lib_html += lib_import[k].format(static=static_path)
    output = '''
    <!DOCTYPE html>
    <html lang="en">
    <head>

        <title>{title} - Dropsignal</title>
        <meta charset="utf-8">
        {lib}

        <link href="{static}css/main.css" rel="stylesheet" type="text/css">

        {scripts}
    </head>'''.format(static=static_path, title=title, scripts=script_html, lib=lib_html)

    output += '<body>'
    if loader:
        output += '''<div class='splash' id='wait'><svg width="300px", height="170px">
    <defs>
    <filter id="blurFilter4">
    <feGaussianBlur in="SourceGraphic" stdDeviation="1.5" />
    </filter>
</defs>
  <rect width="320" height="70" x="-10" y="65" id="tube" />
  <rect width="5" height="135" x="150" class="laser" />
  <rect width="20" height="20" x="142.5" />
  <circle cx="-40" cy="100px" r="30px" class="droplet"/>
  <circle cx="30" cy="100px" r="30px" class="droplet"/>
  <circle cx="100" cy="100px" r="30px" class="droplet" id="first"/>
  <circle cx="170" cy="100px" r="30px" class="droplet" />
  <circle cx="240" cy="100px" r="30px" class="droplet" />
  <circle cx="310" cy="100px" r="30px" class="droplet" />
</svg>
<script>var timeline = anime.timeline({loop:true})
var path = anime.path('#follow')
var droplets =  document.querySelectorAll('#wait .droplet')
timeline.add({
    targets: droplets,
    duration: 3000,
    easing:'linear',
    translateX: 70
})
    .add({
        targets: document.querySelectorAll('#wait #first'),
        duration: 2000,
        fill:  [
            {value: '#C24231'},
            {value: '#C29631'},
            {value: '#e7f5f8'}
        ],
        easing:'linear',
        offset: 1000
    })
    $( window ).on( 'load', function() {
        $('#wait').hide()
        })
</script>'''

    output += '''

    </div>
    <header class="navbar navbar-default">
    <div class="navbar-header">
    <a class="navbar-brand" href="/">Dropsignal</a>
    <ul class="nav navbar-nav">'''

    if links is None:
        links = [("Dynamics", "index.html"),
                 ("Scatter", "scatter.html"),
                 ("Signal", "signal.html"),
                 ("Log", "log.html")]
    for name, href in links:
        cls = 'class="active"' if active == name else ''
        output += ' <li {}><a href="{}">{}</a></li>'.format(cls, href, name)

    if plugins is not None:
        cls = 'active' if active == 'Plugins' else ''
        output += """
            <li class="dropdown {}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Plugins <span class="caret"></span></a>
              <ul class="dropdown-menu">""".format(cls)
        for name in plugins:
            output += (' <li><a href="plugin?name={}">{}</a></li>'
                       '').format(name,
                                  str.capitalize(name.replace("_", " ")))
        output += "</ul></li>"

    output += '''
    </ul>
    </div>
    </header>'''
    return output

def footer():
    """ Return HTML footer"""
    return """
    <footer class="footer">
        <div class="container">
        <p class="text-muted">
            Page generated by <a href="https://gitlab.com/evomachine/dropsignal">dropsignal</a> v{} on {} | <a href="/citation/">Citation</a>
        </p>
    </div>
    </body>
    </html>'""".format(__version__, time.asctime())

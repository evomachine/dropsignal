"""data_interface.py ~ web application serving the data
dropsignal - Guilhem Doulcier 2018 GPL3+

A cherrypy plugin that load and keep the data.
"""

import os
import logging
import json

import pandas as pd
from cherrypy.process import plugins

import dropsignal.protocol
logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
logger.propagate = False

class DataPlugin(plugins.SimplePlugin):
    """ The data is loaded from files and stored in this object,
    The request ask data to this object via cherrypy's bus"""
    def __init__(self, path, bus, channel_prefix):
        plugins.SimplePlugin.__init__(self, bus)
        self.path = path

        self.dynamics = None
        self.protocol = None
        self.runs = None
        self.droplets = None

        # List of messages and actions.
        self.listener = [(channel_prefix+"get_dynamics", self.get_dynamics)]

    def get_dynamics(self, id_exp=None, aggregate=None, timeframe=None, runs=None, columns=None):
        """A data request within the `dynamics` table.

        Args:
           id_exp: list of droplet id_exp to retreive.
           aggregate: column over which mean and standard deviation will be return.
           timeframe: only retreive data within (tmin, tmax), any can be None.
           runs: list (or id) of runs to retreive.
           columns: list of columns to retreive.

        Return a pandas.dataframe."""
        self.bus.log(('Request data for id_exp={}, aggregated on {}, '
                      'timeframe {}, runs {}, columns {}').format(id_exp,
                                                                  aggregate,
                                                                  timeframe,
                                                                  runs, columns))
        if id_exp is not None:
            answer = self.dynamics.query('id_exp in @id_query', local_dict={'id_query': id_exp})
        else:
            answer = self.dynamics
        self.bus.log('dataset is {} answer is {}'.format(self.dynamics.shape, answer.shape))

        if runs is not None:
            try:
                runs.__iter__
            except AttributeError:
                answer = answer.query('run==@runs', local_dict={'runs':runs})
            else:
                answer = answer.query('run in @runs', local_dict={'runs':runs})
        self.bus.log('dataset is {} answer is {}'.format(self.dynamics.shape, answer.shape))

        if timeframe is not None:
            timeframe[0] = answer.time.min() if timeframe[0] is None else timeframe[0]
            timeframe[1] = answer.time.max() if timeframe[1] is None else timeframe[1]
            answer = answer.query('@mn<time<@mx', local_dict={'mn': timeframe[0],
                                                              'mx': timeframe[1]})

        if columns is not None:
            columns = list(set(columns + ['id_exp']))
            answer = answer[columns]

        if aggregate is not None:
            grouped = answer.groupby([aggregate, 'run'])
            mean = grouped.mean().rename(columns=lambda x: x+'_mean')
            std = grouped.std().rename(columns=lambda x: x+'_std')
            answer = pd.merge(mean, std, left_index=True, right_index=True).reset_index()

        self.bus.log('dataset is {} answer is {}'.format(self.dynamics.shape, answer.shape))

        return answer

    def start(self):
        """Called by cherry py when the plugin is started.
        Load all the data to RAM and register the data getter.
        """
        # Register the data getter to the bus.
        for message, action in self.listener:
            self.bus.subscribe(message, action)

        # Load protocol
        try:
            with open(os.path.join(self.path, 'analysis', 'metadata.json'), 'r') as file:
                self.protocol = json.load(file)
        except Exception as ex:
            logger.warning('Failed to load metadata.json ({}). Loading default protocol.'.format(ex))
            self.protocol = dropsignal.protocol.default_protocol()

        # droplet_dynamics contains the annotated droplets whereas
        # signal_annotation contains info about all the peaks, even
        # the ones that were discarded by quality control.
        try:
            self.dynamics = pd.read_csv(os.path.join(self.path, 'analysis', 'droplet_dynamics.csv'))
            if "droplet_id" not in self.dynamics.columns:
                self.dynamics['droplet_id'] = self.dynamics['id_exp'] + 1
        except FileNotFoundError:
            logger.error('Droplet dynamics not found (droplet_dynamics.csv)')
            self.dynamics = None

        try:
            droplets = pd.read_csv(os.path.join(self.path, 'analysis', 'droplet.csv'))
            self.droplets = droplets[['droplet', 'records', 'well', 'group', 'tags']]
            self.dynamics = pd.merge(self.dynamics, droplets[['droplet', 'well', 'group']],
                                     left_on='id_exp', right_on='droplet')
        except FileNotFoundError:
            logger.error('Droplet dynamics not found (droplet.csv)')
        except Exception as e:
            logger.error('Failed droplet.csv: {}'.format(e))

        try:
            self.runs = pd.read_csv(os.path.join(self.path, 'analysis', 'runs.csv'))
        except FileNotFoundError:
            logger.error('Run quality  not found (runs.csv)')
        except Exception as e:
            logger.error('Failed runs.csv: {}'.format(e))

    def stop(self):
        """Properly shut down the plugin"""
        for message, action in self.listener:
            self.bus.unsubscribe(message, action)
        self.unsubscribe()
        self.bus.log('Shutting down data plugin')

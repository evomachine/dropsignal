"""web ~ web application serving the data
This file is part of the dropsignal package.
Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+

This module control the web-interface to dropsignal.

The interface is based on a webserver (cherrypy) that is started with
the command `dropsignal_web` (or running the windows executable). This
interface is an instance of the App "Dashboard" found in the module
web.dashboard that list all experiments found in the path.

From the dashboard, protocols can be edited, the pipeline can be
started and experiment can be served (by starting Experiments
instances).

Submodules are:

web.dashboard: the dashboard app (experiment index).
web.experiment: an app serving one experiment
web.protocol_editor: interface to edit the protocol.json
web.pipeline_runner: starts the dropsignal pipeline in a thread
web.html: html template.
web.raw_explorer: interface for raw signal.
web.data_interface: interface for processed signal.
"""
import logging
import os
import webbrowser
import socket
import argparse
import mimetypes
import subprocess

import cherrypy
import cherrypy.process.plugins

from dropsignal import __version__, APP_PATH
import dropsignal.protocol
from dropsignal.web.dashboard import Dashboard

WEB_OPTIONS = dropsignal.protocol.web_options()
logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
cherrypy.log.error_log.propagate = True
cherrypy.log.access_log.propagate = True
mimetypes.types_map['.json'] = 'text/json'
mimetypes.types_map['.xlsx'] = 'application/x-download'


def main():
    """Entry point for dropsignal_web"""
    print('Welcome in dropsignal web ({}) !  '.format(__version__))

    # Parse arguments from the command line
    parser = argparse.ArgumentParser(description='Dropsignal web user interface')
    parser.add_argument('--port', default=WEB_OPTIONS['web_initial_port'],
                        help="Server port")
    parser.add_argument('--public', action='store_true',
                        default=WEB_OPTIONS['web_public'],
                        help="Serve on public IP")
    parser.add_argument('--browser',
                        default=WEB_OPTIONS['web_browser'],
                        action='store_true',
                        help=("Open browser (dropsignal works best "
                              "with firefox and will try it in priority)"))
    parser.add_argument('--log', action='store_true', help="Webserver logging")
    parser.add_argument('path', metavar='PATH', type=str,
                        default=WEB_OPTIONS['web_default_path'], nargs='*',
                        help='Raw data location.')
    args = parser.parse_args()

    if args.path:
        os.chdir(os.path.expanduser(args.path[0]))

    # Redirect the logging in the console.
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
    logger.addHandler(console_handler)

    # Start the dashboard app and scan the folder given in the command line.
    dash = Dashboard(args.path)

    # Mount the app
    conf = {
        '/static':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(APP_PATH, 'static'),
            'tools.expires.on': True,
            'tools.expires.secs': 0

        },
        '/favicon.ico':
        {
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.join(APP_PATH, 'static', 'favicon.ico')
        }
    }
    cherrypy.tree.mount(dash, '/', conf)

    # Start the web server.
    start_server(port=int(args.port),
                 open_webbrowser=args.browser,
                 public=args.public,
                 log=args.log)


def check_port(port, skt=None):
    """ Check if `port` is in use using the socket `skt`"""
    if skt is None:
        skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        skt.bind(("127.0.0.1", port))
    except OSError as ex:
        # On windows, a OSError "winerror 10048" is thrown if the
        # port is in use.
        logger.info("Port {} is already in use: {}".format(port, ex))
        return False
    except socket.error as ex:
        if ex.errno == 98:
            logger.info("Port {} is already in use".format(port))
            return False
        else:
            raise ex
    else:
        return True


class Br(object):
    """ Ugly hack to start the firefox on windows """

    def open(self, url):
        """ Open the firefox at url """
        logger.debug('Calling Firefox from {}'.format(WEB_OPTIONS["web_firefox_path"]))
        subprocess.call('"{}" {}'.format(WEB_OPTIONS["web_firefox_path"], url))


def start_server(port=None, open_webbrowser=False, public=False, log=False):
    """ Start the web server.

    Args:
        port: first port to try (will try port up to port+10).
        open_webbrowser (bool): if true try to open the webbrowser on the server.
        public (bool): if true serve on public ip otherwise just on localhost.
        log (bool): Cherrypy logging
    """

    # Try to find an open port.
    port = int(port)
    iport = port
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as skt:
        while not check_port(port, skt) and port < iport+10:
            port += 1
        if port >= iport+10:
            logger.critical('Failed to find an open port.')
            return False

    # Configure and start cherrypy
    cherrypy.config.update({
        'server.socket_port': int(port),
        'server.socket_host': '0.0.0.0' if public else '127.0.0.1',
        'log.screen': log,
        'tools.staticdir.debug': log, })

    # Open Web-Browser.
    if open_webbrowser:
        try:
            browser = webbrowser.get('firefox')
        except webbrowser.Error:
            if os.path.exists(WEB_OPTIONS['web_firefox_path']):
                # Ugly hack for windows.
                browser = Br()
            else:
                logger.warning(('Unable to find Firefox, will use default browser instead. '
                                'Dropsignal works best with Mozilla Firefox !'))
                browser = webbrowser.get(None)
        cherrypy.engine.start_with_callback(browser.open,
                                            ('http://localhost:{}'.format(port),))
    else:
        cherrypy.engine.start()

    logger.info("Serving {} on http://{}:{}".format('publicly' if public else '',
                                                    '127.0.0.1' if not public else '0.0.0.0',
                                                    port))
    return port

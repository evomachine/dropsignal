"""Dashboard - UI listing the experiments.
This file is part of the dropsignal package.
Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
"""

import glob
import os
import logging
import itertools
import threading

import cherrypy

from dropsignal import __version__
import dropsignal.web.protocol_editor
from dropsignal.web.experiment import Experiment, RawExperiment
from dropsignal.web.pipeline_runner import PipelineRunner
import json

logger = logging.getLogger('dropsignal')


class Dashboard(object):
    """ Cherrypy app provinding the list of experiments and buttons to start the pipeline """

    def __init__(self, paths_to_scan):
        self.app = []
        self.discovered = {}
        self.mount_set = set()
        self.runner = None
        self.paths_to_scan = paths_to_scan

    def discover(self, path):
        """Search the `path` for millifluidic experiments folders (look for Raw*.raw files)"""
        files = glob.glob(os.path.join(path, '**', 'Raw*.raw'), recursive=True)
        number = 0
        for f in files:
            key = os.path.dirname(f)
            analysis_folder = os.path.join(os.path.dirname(f), 'analysis')
            if key not in self.discovered:

                self.discovered[key] = {'path': os.path.dirname(f),
                                        'started': False,
                                        'analysis': os.path.exists(analysis_folder)}
                number += 1
            else:
                self.discovered[key]['analysis'] = os.path.exists(analysis_folder)
            if self.discovered[key]['analysis']:
                try:
                    with open(os.path.join(analysis_folder, 'metadata.json'), 'r') as meta:
                        self.discovered[key]['version'] = json.load(
                            meta)['pipeline_infos']['version']
                except Exception as ex:
                    self.discovered[key]['version'] = "error: {}".format(type(ex))
        logger.info('Scanning {}... {} new experiments found'.format(path, number))

    @cherrypy.expose
    def edit(self, path=None):
        """ Start the protocol editor """
        if path is None:
            return 'You must provide a path'
        return dropsignal.web.protocol_editor.webui(self.discovered[path])

    @cherrypy.expose
    def save(self, action, **kargs):
        """ Save the protocol """
        head = dropsignal.web.html.header('/static/',
                                          scripts=[],
                                          inline_script='''
                                          window.setTimeout(function() {{
                                          location.href = "/"
                                          }}, 1000)
                                          ''',
                                          title='Saving protocol',
                                          active='home',
                                          links=[('Home', '/'), ('Quit', '/stop')])
        if action == 'reset':
            try:
                pr = dropsignal.web.protocol_editor.save(
                    {'save_path': kargs['save_path']})
                content = '''
                <main class='container'>
                <div class="alert success"><h1> Protocol cleared </h1>
                <pre>{}</pre>
                <a href="/">Return</a>.</div>
                </main>'''.format(pr)
            except Exception as ex:
                content = '''
                <main class='container'>
                <div class="alert danger"><h1> An error occured: {} </h1>
                <a href="/">Return</a>.</div>
                </main>'''.format(ex)
                raise ex
        else:
            try:
                pr = dropsignal.web.protocol_editor.save(kargs)
                content = '''
                <main class='container'>
                <div class="alert success"><h1> Protocol saved </h1>
                <pre>{}</pre>
                <a href="/">Return</a>.</div>
                </main>'''.format(pr)
            except Exception as ex:
                content = '''
                <main class='container'>
                <div class="alert danger"><h1> An error occured: {} </h1>
                <a href="/">Return</a>.</div>
                </main>'''.format(ex)
                raise ex
        return head + content + dropsignal.web.html.footer()

    @cherrypy.expose
    def run(self, path=None):
        """ Run the pipeline on the experiment in `path`"""
        if self.runner is not None:
            self.runner.stop()
            del self.runner
            self.runner = None
        self.runner = PipelineRunner(cherrypy.engine, self.discovered[path])
        self.runner.start()
        head = dropsignal.web.html.header('/static/',
                                          scripts=[],
                                          inline_script='''
                                          var http
                                          function update(){
                                          var wait = d3.select('#wait').attr('class','nosplash').style('display', 'block')
                                          d3.select('#anim').node().appendChild(wait.node())
                                          if (http.readyState == 4) {

                                          var response = http.responseText
                                          var logframe = document.getElementById('logframe')
                                          logframe.innerHTML = response

                                          $(logframe).animate({ scrollTop:  logframe.scrollHeight }, 1000);

                                          if (response.slice(-7)=='success'){
                                          d3.select('#done_div').style('display','block')
                                          window.setTimeout(function() {document.getElementById("done").submit()}, 2000)
                                          }

                                          setTimeout(refresh, 1000)
                                          }
                                          }

                                          function refresh() {
                                          http = new XMLHttpRequest()
                                          http.open('get', '/logger/')
                                          http.timeout = 1000
                                          http.onreadystatechange = update
                                          http.send(null)
                                          }

                                          refresh()
                                          ''',
                                          title='Running Dropsignal',
                                          active='home',
                                          links=[('Home', '/'), ('Quit', '/stop')])

        return head + '''
        <main class='container'>
        <div id="anim">        <h1> Running Dropsignal... </h1> </div><div id="logframe"> </div>
        <div id="done_div" style="display:none;" class='alert alert-success'>
        Done ! <form id="done" action="/start_exp/" method="post">
        <input type="hidden" name="path" value="{path}">
        <input class="btn btn-success" type="submit" value="Open the results"></form></div>
        </main>
        '''.format(path=path) + dropsignal.web.html.footer()

    @cherrypy.expose
    def logger(self):
        """ Display the log of the experiment as it is running """
        if self.runner is not None:
            try:
                txt = self.runner.logger()
            except Exception:
                raise cherrypy.HTTPError(404, "Error in runner")
        else:
            raise cherrypy.HTTPError(404, "No runner")
        return txt

    @cherrypy.expose
    def index(self, path=None):
        """ Display the list of experiment. This is the landing page of dropsignal_web. """

        if path is None:
            path = os.getcwd()
        else:
            self.paths_to_scan.append(path)

        for pth in self.paths_to_scan:
            self.discover(pth)

        head = dropsignal.web.html.header('static/',
                                          scripts=[],
                                          title='Dashboard',
                                          active='home',
                                          links=[('Home', '/'), ('Quit', '/stop')])

        form = """
        <form action="/" method="post">
        <label> Find more experiments in: </label>
        <input name="path" style="width:60%; min-width:50em" value={}>
        <input class="btn btn-default" type="submit" value="Scan"></form> """.format(path)

        if len(self.discovered):
            exp_list = """
            <h3> Experiments </h3>
            <table class='table'>
            <tr><th>Path</th><th>Edit</th><th>Run</th><th>Results</th></tr>
            """
            data = sorted(self.discovered.items(),
                          key=lambda x: os.path.dirname(x[1]['path'])+x[1]['path'])
            for key, group in itertools.groupby(data, lambda x: os.path.dirname(x[1]['path'])):
                exp_list += '<tr><th>{}</th><th></th><th></th><th></th></tr>'.format(key)
                exp_list += "</tr>\n<tr>".join([exp_line(**dict(grp))
                                                for _, grp in group])
            exp_list += '</table>'

        else:
            exp_list = 'No experiment found in {}'.format(path)

        main = "<main class='container'>" + exp_list + form + "</main>"
        return head + main + dropsignal.web.html.footer()

    @cherrypy.expose
    def stop_exp(self, path=None):
        """ Stop the Experiment app in `path`"""
        if path is None:
            return 'You must provide a path'
        self.discovered[path]['app'].teardown()
        del self.discovered[path]['app']
        self.discovered[path]['started'] = False
        self.mount_set.remove(self.discovered[path]['mount_point'])
        head = dropsignal.web.html.header('/static/',
                                          scripts=[],
                                          inline_script='''
                                          window.setTimeout(function() {{
                                          location.href = "/"
                                          }}, 1000)
                                          ''',
                                          title='Closing',
                                          active='home',
                                          links=[('Home', '/'), ('Quit', '/stop')])
        content = '''
            <main class='container'>
            <h1> Results closed. </h1>
            <a href="/">Return</a>.
            </main>'''
        return head + content + dropsignal.web.html.footer()

    @cherrypy.expose
    def start_exp(self, path=None, raw=False):
        """ Start the Experiment app in `path`"""
        if path is None:
            return 'You must provide a path'
        try:
            # Start the app.
            if not raw:
                self.discovered[path]['app'] = Experiment(os.path.expanduser(path))
            else:
                self.discovered[path]['app'] = RawExperiment(os.path.expanduser(path))

            # Get the mounting point, taking in account that several
            # folder can have the same name.
            self.discovered[path]['mount_point'] = '/'+os.path.basename(path)
            i = 0
            while self.discovered[path]['mount_point'] in self.mount_set:
                self.discovered[path]['mount_point'] += str(i)
                i += 1
            self.mount_set.add(self.discovered[path]['mount_point'])

            # Mount the app.
            cherrypy.tree.mount(self.discovered[path]['app'],
                                self.discovered[path]['mount_point'],
                                self.discovered[path]['app'].conf)

        except Exception as ex:
            return 'Error encountered when starting the app: {}, path={}'.format(ex, path)
        else:
            self.discovered[path]['started'] = True
            head = dropsignal.web.html.header('/static/',
                                              scripts=[],
                                              inline_script='''
                                              window.setTimeout(function() {{
                                              location.href = "{0}/"
                                              }}, 1000)
                                              '''.format(self.discovered[path]['mount_point']),
                                              title='Running Dropsignal',
                                              active='home',
                                              links=[('Home', '/'), ('Quit', '/stop')])
            content = '''
            <main class='container'>
            <h1> Loading... </h1>
            <a href="{0}/index.html">See the results</a>
            '''.format(self.discovered[path]['mount_point'])
            return head + content + '</main>' + dropsignal.web.html.footer()

    @cherrypy.expose
    def stop(self):
        """ Close dropsignal, terminate the process. """
        threading.Timer(1, lambda: os._exit(0)).start()
        return 'Dropsignal is closing. Goodbye !'

    @cherrypy.expose
    def citation(self):
        """Display citation information"""
        return """
 <html>
<body>
You can cite this software as:

<p>
  Doulcier, G. 2019. Dropsignal - Millifluidic droplet trains
  analysis. <a href="https://dx.doi.org/10.5281/zenodo.1164108">doi:10.5281/zenodo.1164108</a>
</p>

BibTeX (for citation managers)
<pre>
@article{Doulcier2019,
  doi = {doi:10.5281/zenodo.1164108},
  url = {http://dx.doi.org/10.5281/zenodo.1164108},
  year  = {2019},
  month = {feb},
  author = {Guilhem Doulcier},
  title = {Dropsignal - Millifluidic droplet trains analysis},
}
</pre>
</body>
</html>
 """


def exp_line(path, started, analysis, mount_point=None, version=None, **_):
    """ Write down one line of the experiment table in the dashboard.

    Args:
        path (str): Path of the experiment.
        started (bool): is an instance of Experiment already serving this ?
        analysis (bool): do we already have an analysis folder ?
        mount_point (str): where the Experiment app is mounted.
    """

    template = """<td>{path}</td> <td><form action="/edit/" method="post">
        <input type="hidden" name="path" value="{path}">
        <input class="btn-default btn" type="submit" value="Edit the protocol"></form></td>"""
    if not started:
        template += """<td><form action="/run/" method="post">
        <input type="hidden" name="path" value="{path}">
        <input class="btn-default btn" type="submit" value="Run the analysis"></form></td>"""
    else:
        template += """<td></td>"""

    if not started:
        if analysis:
            template += """<td><form action="/start_exp/" method="post">
            <input type="hidden" name="path" value="{path}">"""
            if __version__ == version:
                template += '<input class="btn-primary btn" type="submit" value="Open the results"></form></td>'
            else:
                template += '<input class="btn-warning btn" type="submit" value="Open the results (old version)"></form></td>'

        else:
            template += """<td><form action="/start_exp/" method="post">
            <input type="hidden" name="path" value="{path}">
            <input type="hidden" name="raw" value="true">
            <input class="btn-defaul btn" type="submit" value="Open the Raw signal"></form></td>"""
    else:
        template += """<td><form action="/stop_exp/" method="post">
        <a class="btn-success btn btn-group" href="{mount_point}">Go to results</a>
        <input type="hidden" name="path" value="{path}">
        <input class="btn-danger btn btn-group" type="submit" value="Close the results"></form></td>"""

    return template.format(path=path, mount_point=mount_point)

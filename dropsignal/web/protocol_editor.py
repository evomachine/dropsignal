"""protocol.editor ~ interactive interface for the edition of protocol files
This file is part of the dropsignal package.
dropsignal - Guilhem Doulcier 2018 AGPL3+
"""
import json
import os
from itertools import chain
from hashlib import sha1
from xml.sax.saxutils import quoteattr
import logging

import cherrypy

import dropsignal.protocol.reader
import dropsignal.processing.binraw as binraw
import dropsignal.processing.signal as signal
from dropsignal.web.raw_explorer import RawDataPlugin
import dropsignal.web.html

logger = logging.getLogger('dropsignal')


class DetectionTest(object):
    """A cherrypy small app that start the RawDataPlugin on path and
    expose an interface to perform droplet detection from javascript."""

    def __init__(self, path):
        self.conf = {'/': {
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'application/json')],
        }}
        self.experiment = path

        # Start the raw data plugin holding the data.
        self.channel_prefix = sha1(str(path+'protocol').encode('utf-8')).hexdigest()
        self.plugin = RawDataPlugin(cherrypy.engine, path, self.channel_prefix)
        self.plugin.start()
        self.plugin.subscribe()

    @cherrypy.expose()
    @cherrypy.tools.json_out()
    def detect(self, run, window, window_size, threshold, buffer_drops_max_number, buffer_drops_waiting_time, ptd=None, advanced=None, active_channels=None):
        """ Return the number of peaks in a raw signal.
        /static/js/protocol_editor.js request this interface.
        """
        advanced = json.loads(advanced)
        active_channels = json.loads(active_channels)
        peaks = {}

        if ptd is None:
            ptd = list(frozenset([binraw.PMT2PTD[pmt]
                                  for pmt in active_channels]))
        elif ptd[0] == 'p':
            ptd = (ptd,)

        out = {
            'ptd': ptd,
            'request': {'run': run,
                        't': threshold,
                        'args': (window, window_size, threshold, buffer_drops_max_number,
                                 buffer_drops_waiting_time, ptd, advanced, active_channels)}}

        try:
            data = cherrypy.engine.publish(self.channel_prefix+'get_data', int(run))
        except Exception as ex:
            out['error'] = 'Failed to load signal for run {}: {}'.format(run, str(ex))
            return out
        if data is None or data[0] is None:
            out['error'] = 'Failed to load signal for run {}: {}'.format(run, data)
            return out
        data = data[0]
        default_param = dict(window=window,
                             window_size=int(window_size),
                             threshold=float(threshold))
        for p in ptd:
            param = default_param.copy()
            use_advanced = False
            if p in advanced:
                for k in default_param.keys():
                    if k in advanced[p]:
                        param[k] = advanced[p][k]
                        use_advanced = True
            logger.info('Detection preview for channel {} run {} - {}'.format(p, run, param))
            try:
                peaks[p] = signal.detect_peaks(data[p],
                                               **param)
                peaks[p] = signal.remove_buffer_drops(*peaks[p], data['time'],
                                                      buffer_number=int(
                                                          buffer_drops_max_number),
                                                      buffer_waiting_time=float(buffer_drops_waiting_time))
                try:
                    out[p] = {'number': peaks[p].shape[0], 'advanced': use_advanced}
                except AttributeError as ex:
                    out[p] = {'number': len(peaks[p][0]), 'advanced': use_advanced}
            except Exception as ex:
                out['error'] = 'Detection on ptd {} failed: {}, args: {}'.format(
                    p, str(ex), out['request']['args'])
        return out


def webui(experiment):
    """ Return the HTML form to edit the protocol in experiment"""

    # Start the interactive detection plugin.
    path = experiment['path']
    app = DetectionTest(path)
    cherrypy.tree.mount(app, '/data', app.conf)
    message_class = "success"
    protocol_path = os.path.join(path, 'protocol.json')
    template_path = os.path.join(path, 'template.csv')

    # Load Protocol
    try:
        protocol = dropsignal.protocol.reader.protocol(protocol_path)
    except Exception as ex:
        protocol_msg = ("<strong> Protocol loading failed </strong> "
                        "{} from {}").format(ex, protocol_path)
        protocol = dropsignal.protocol.default_protocol()
        message_class = "danger"
    else:
        protocol_msg = ("<strong> Protocol </strong> "
                        "({} entries) successfully loaded"
                        "from {}").format(len(protocol), path)

    # Load template
    try:
        template = dropsignal.protocol.reader.template(template_path)
    except Exception as ex:
        template_msg = ("<strong> Template loading failed </strong> "
                        "{} from {}").format(ex, template_path)
        message_class = "warning"
        template = dropsignal.protocol.default_template()
    else:
        template_msg = ("<strong> Template </strong> "
                        "({} lines) successfully loaded"
                        " from {}").format(template.shape[0], path)

    # Write the status message box.
    message = ('<div class="alert alert-{}" role="alert">'
               '<p>{}</p>'
               '<p>{}</p>'
               '</div>').format(message_class, protocol_msg, template_msg)

    # Prepare the HTML header.
    header = dropsignal.web.html.header('/static/',
                                        scripts=['protocol_editor', 'legend', 'plate'],
                                        title='Dropsignal Dashboard',
                                        active='home',
                                        lib=['jquery', 'd3v4', 'bootstrap'],
                                        links=[('Home', '/'), ('Quit', '/stop')])

    # Compare with default...
    entries = list_entries(protocol)

    # Generate the form for the protocol
    protocol_form = html_protocol(entries, path)

    # Generate the form for the template
    template_form = html_template(template)

    # Return the web page
    return (header
            + '<main class="container"><h1> Protocol Editor </h1>'
            + message
            + protocol_form
            + template_form
            + '</main>'
            + dropsignal.web.html.footer())


def save(protocol):
    """ Save the result of the POST request in the right path.
    Only write the values that are different from default."""

    # The path of the protocol file has been added in a hidden input.
    # Retreive and delete it.
    path = os.path.join(protocol['save_path'], 'protocol.json')
    del protocol['save_path']

    # Empty protocols are returned immediatly
    if not bool(protocol):
        content = '# Empty configuration file from dropsignal'
        open(path, 'w').write(content)
        return content

    # input type=checkbox are only posted if they are checked.
    # to circumvent this there is a hidden _checkbox.KEY field for each checkbox.
    # We have to remove them and set the checkbox to false if necessary.
    hidden_keys = [k for k in protocol.keys() if k[:10] == "_checkbox."]
    for k in hidden_keys:
        if k[10:] not in protocol:
            protocol[k[10:]] = json.dumps(False)
        del protocol[k]

    # Create the file...
    content = ["# Configuration file from dropsignal.",
               "# Automatically generated by the web ui: you should probably not edit this."]

    modified = dropsignal.protocol.compare_with_default(protocol)

    content += ['"{}":{}'.format(key, value)
                for key, value
                in sorted(protocol.items())
                if modified[key]]
    content = '\n'.join(content)

    # Write the file
    open(path, 'w').write(content)

    # Check correctness
    try:
        dropsignal.protocol.reader.protocol(path)
    except Exception as ex:
        raise ValueError('The new protocol is incorrect: {}'.format(ex))
    return content


def list_entries(protocol, default=None, spec=None):
    """Prepare the protocol entries for the html template."""
    entries = {}
    if spec is None:
        spec = dropsignal.protocol.get_spec()
    if default is None:
        default = dropsignal.protocol.default_protocol()
    modified = dropsignal.protocol.compare_with_default(protocol, default)
    for key in default.keys():
        value = protocol[key] if modified[key] else default[key]
        if key in spec:
            description = spec[key]['description'] if 'description' in spec[key] else ''
            help_text = spec[key]['help_text'] if 'help_text' in spec[key] else ''
            if spec[key]['category'] not in entries:
                entries[spec[key]['category']] = []
            entries[spec[key]['category']].append((key, value,
                                                   modified[key],
                                                   description, help_text))
        else:
            if 'misc' not in entries:
                entries['misc'] = []
            entries['misc'].append((key, value, True, '', ''))
    return entries


def html_protocol(entries, save_path):
    """ Generate the HTML form to edit the protocol"""
    content = '<form method="post" id="protocol_form" action="/save">'

    # Hidden variables.
    content += ('<input type="hidden" id="form_action" name="action" value="save"/>\n'
                '<input name="save_path" type="hidden" value="{}"/>').format(save_path)

    # Save button
    content += ('<div id="control"> '
                '<input class="btn btn-success" type="submit" value="Save"/>'
                '</div>')

    for category, entries_from_cat in sorted(entries.items()):
        category_key = category.replace(' ', '_').replace('.', '').lower()
        folded = category_key != 'detection'
        button = ('<button class="btn btn-default"'
                  'type="button" data-toggle="collapse"'
                  'data-target="#param_category_{0}"'
                  'aria-expanded="{1}"'
                  'aria-controls="param_category_{0}">'
                  'Show </button>').format(category_key, 'true' if not folded else 'false')
        content += '<h3>{}{}</h3>'.format(category, button)
        content += ('<div id="param_category_{}" '
                    'class="collapse well {}">').format(category_key,
                                                        'in' if not folded else '')

        for key, value, modified, description, help_text in sorted(entries_from_cat, key=lambda x: x[0]):
            content += html_widget(key, value, modified, description, help_text)

        if category_key == 'detection':
            # Prepare the spot for the interactive detection widget.
            # it will be filled by javascript.
            content += '<div id="detect"></div>'

        content += '</div>'

    # Another save button at the bottom.
    content += ('<div id="control_foot">'
                '<input class="btn btn-success" type="submit" value="Save">'
                '</div></form>')
    return content


def html_widget(key: str, value, modified: bool, description: str, help_text: str = None):
    """ Return the HTML widget that edit the parameter key"""
    html = '<div class="form-group {}" '.format('modified' if modified else '')
    html += 'id="edit_{0}"><label>{0}:</label> <input name="{0}"'.format(key)
    if isinstance(value, bool):
        html += 'type="checkbox" {} value="true"/>'.format('checked' if value else '')
        html += '<input type="hidden" name="_checkbox.{}"/>'.format(key)
    elif isinstance(value, int):
        html += 'type="number" step="1" value="{}"/>'.format(value)
    elif isinstance(value, float):
        html += 'type="number" step="0.01" value="{}"/>'.format(value)
    else:
        html += 'class="form-control" type="text" value={}/>'.format(
            quoteattr(json.dumps(value)))
    html += '<p class="details">{}</p>'.format(description)
    html += '<p class="verbose details">{}</p>'.format(help_text)
    html += '</div>'
    return html


def html_template(template):
    """Return an HTML representation of the template"""
    html = '<h3>Template</h3>'
    # TODO: use the color form the protocol instead.
    colors = ['#4e79a7', "#f28e2b", "#e15759", "#76b7b2", "#59a14f",
              "#edc948", "#b07aa1", '#ff9da7', "#9c755f", "#bab0ac"]
    droplets = list(
        chain(*[[{'well': r.well, }]*r.droplet_number for _, r in template.iterrows()]))
    group_color = {key: colors[i % len(colors)]
                   for i, key in enumerate(template.description.unique())}
    well_color = {r.well: group_color[r.description] for _, r in template.iterrows()}
    groups = [{'label': key, 'id': [i for i in val.well]}
              for key, val in template.groupby('description')]
    html += '''
    <div id="legend" style="float:right"></div>
    <div id="plate"></div>
    <script>
    $(document).ready(function(){{
    var droplets = {};
    var well_color = {};
    var color_group = {};
    var groups = {};
    plate = new Plate(d3.select('#plate').append('svg'),
 600, 400, droplets, well_color, false);
    legend = new Legend(d3.select('#legend'), groups, color_group);
    }})
    </script>'''.format(json.dumps(droplets), json.dumps(well_color),
                        json.dumps(group_color), json.dumps(groups))
    cols = [x for x in ['well', 'description', 'order', 'droplet_number']
            if x in template.columns]
    template = template[cols].set_index('well')
    try:
        def highlight(s):
            """Highlight the group names"""
            return ['background-color: {}'.format(group_color[v])
                    if v in group_color
                    else ''
                    for v in s]
        html += '\n\n' + \
            template.style.apply(highlight).set_table_attributes('class="table"').render()
    except Exception as ex:
        html += template.to_html(classes="table")
    return html

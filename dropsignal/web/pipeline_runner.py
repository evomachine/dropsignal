"""Dashboard - Plugin in charge of running the pipeline from the web.
This file is part of the dropsignal package.
Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
"""
import os
import re
import concurrent.futures

import cherrypy
import cherrypy.process.plugins

import dropsignal.pipeline

LOG_FORMAT = {'DEBUG':'<span class="label label-default">Debug</span>',
              'INFO':'<span class="label label-primary">Info</span>',
              'CRITICAL':'<span class="label label-danger">Critical</span>',
              'ERROR':'<span class="label label-danger">Error</span>',
              'WARNING':'<span class="label label-warning">Warning</span>'}
LOG_REGEXP = re.compile('|'.join(map(re.escape, LOG_FORMAT.keys())))

def format_log(line):
    """Convert the pipeline log ASCII structure to HTML"""
    if '---' in line:
        return '<h4>'+line.split('---')[1]+'</h4>'
    if '~~~' in line:
        return '<h2>'+line.split('~~~')[1]+'</h2>'
    return LOG_REGEXP.sub(lambda match: LOG_FORMAT[match.group(0)], line)

class PipelineRunner(cherrypy.process.plugins.SimplePlugin):
    """Cherrypy plugin running the pipeline"""
    def __init__(self, bus, experiment):
        cherrypy.process.plugins.SimplePlugin.__init__(self, bus)
        self.experiment = experiment
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)

        # clear the logfile
        self.logpath = os.path.join(self.experiment['path'], 'analysis', 'pipeline_log.txt')
        if not os.path.exists(os.path.dirname(self.logpath)):
            os.mkdir(os.path.dirname(self.logpath))
        open(self.logpath, 'w').close()
        self.running = True
        self.future = None

    def logger(self):
        """Return an HTML representation of the current content of the self.logpath file"""
        content = ''
        end = False
        with open(self.logpath, 'r') as file:
            for line in file:
                content += '<p>'+format_log(line) + '</p>'
                if '~~~ END ~~~' in line:
                    content += '''success'''.format(path=self.experiment['path'])
                    end = True
            if not end  and not self.running:
                exception_text = ''
                try:
                    exception_text = self.future.exception()
                except Exception as ex:
                    exception_text = str(ex)
                else:
                    content += '''<div class='alert alert-danger'>
                    The analysis stopped before completion.
                    <p><strong> Message: </strong>
                    {}
                    </p>
                    <a href='/' class="btn btn-danger"> Return </a></div>
                    '''.format(exception_text)
        return content

    def start(self):
        """ Start the pipeline in a thread pool executor. """
        if self.running:
            self.future = self.executor.submit(dropsignal.pipeline.pipeline,
                                               self.experiment['path'])
            self.future.add_done_callback(self.stop)

    def stop(self, _=None):
        """ Shut down the plugin and the thread pool executor. """
        self.running = False
        try:
            self.executor.shutdown(wait=False)
        except RuntimeError:
            del self.executor

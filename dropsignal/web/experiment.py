"""experiment - Serving one experiment.
This file is part of the dropsignal package.
Copyright 2016-2018 Guilhem Doulcier, Licence GNU AGPL3+
"""
import os
from hashlib import sha1
import re
import json
import logging
import cherrypy
import dropsignal.web.html
from dropsignal.web.pipeline_runner import format_log
import dropsignal.web.raw_explorer
from dropsignal.web.raw_explorer import RawDataPlugin
from dropsignal.web.data_interface import DataPlugin
from dropsignal import APP_PATH

logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
logger.propagate = False

class Experiment(object):
    """CherryPy app - Serve one experiment.

    One instance of this class is created for each experiment that is to be served.

    It exposes the main tabs of the interface in its methods (index,
    signal, log, plugins) which returns the html pages as well as the
    json request (data, signal, signal_annotation).

    It starts two cherrypy plugins that will listen to requests in the
    cherrypy bus (self.rawdata_plugin and self.data_plugin).

    """
    def __init__(self, path):
        self.path = path

        # The channel prefix is used before each message sent to the cherrypy
        # bus for this experiment (to the data and rawdata cherrypy plugins)
        # Allowing to have multiple experiments open without conflict.
        self.channel_prefix = sha1(str(self.path).encode('utf-8')).hexdigest()

        # Cherrypy app config, setup the static content directory for the dropsignal plugins.
        self.conf = {
            '/plugin_static':{
                'tools.staticdir.on':True,
                'tools.staticdir.dir': os.path.join(self.path, 'analysis', 'plugins'),
                'tools.expires.on'    : True,
                'tools.expires.secs'  : 0
            }
        }

        # Load the metadata.json.
        self._metadata = {}
        with open(os.path.join(self.path, 'analysis', 'metadata.json'), 'r') as file:
            self._metadata = json.load(file)

            # Monkey-patching the metadata, this code should be moved to the metadata.json writing.
            self._metadata['droplet_to_well']['-1'] = 'Unknown'
            self._metadata['well_to_group']['Unknown'] = 'Unknown'
            for well in self._metadata['droplet_to_well'].values():
                if well not in self._metadata['well_to_group']:
                    self._metadata['well_to_group'][well] = 'Unknown'

        # Start the plugin serving data
        self.data_plugin = DataPlugin(path=path, bus=cherrypy.engine,
                                      channel_prefix=self.channel_prefix)
        self.data_plugin.subscribe()  # subscribe to start and stop channels of the bus.
        self.data_plugin.start()

        # Start the plugin serving raw singal data.
        self.rawdata_plugin = RawDataPlugin(cherrypy.engine, path,
                                            self.channel_prefix)
        self.rawdata_plugin.subscribe() #subscribe to start and stop channels of the bus.
        self.rawdata_plugin.start()


        # Set the links at the top of each page
        self.plugins = self._metadata['enabled_plugins']
        self.links = [('Home', '/'),
                      ('Close', '/stop_exp?path={}'.format(self.path)),
                      ('Dynamics', 'index'),
                      ('Signal', 'explore_signal'),
                      ('Log', 'log')]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def metadata(self):
        """ Return the content of analysis/metadata.json """
        return self._metadata

    @cherrypy.expose
    @cherrypy.tools.accept(media='application/json')
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def data(self):
        """Return reconstructed dynamics. (Interrogate the DataPlugin)."""
        param = {'id_exp':None, 'aggregate':None, 'timeframe':None, 'runs':None, 'columns':None}
        param.update(cherrypy.request.json)

        # Request to self.data_plugin
        data = cherrypy.engine.publish(self.channel_prefix+'get_dynamics',
                                       param['id_exp'],
                                       param['aggregate'],
                                       param['timeframe'],
                                       param['runs'],
                                       param['columns'])[0]
        return data.fillna(0).to_dict(orient='records')

    @cherrypy.expose
    @cherrypy.tools.accept(media='application/json')
    @cherrypy.tools.json_out()
    def signal_annotation(self, run="list"):
        """ Return the number and positon of peaks and the length of raw signal.
        (Interrogate the RawDataplugin)"""
        return dropsignal.web.raw_explorer.signal_annotation_request(run=run,
                                                                     channel_prefix=self.channel_prefix)


    @cherrypy.tools.accept(media='application/json')
    @cherrypy.tools.json_out()
    @cherrypy.expose
    def signal(self, t0=None, tf=None, run=None, channel=None, skip=1, drop=None, peak=None):
        """ Return a chunck of raw signal.  (Interrogate the RawDataplugin) """
        return dropsignal.web.raw_explorer.signal_request(t0=t0, tf=tf, run=run,
                                                          channel=channel,
                                                          skip=skip, drop=drop,
                                                          peak=peak,
                                                          channel_prefix=self.channel_prefix)


    @cherrypy.expose
    def index(self, **_):
        """Dynamics tab ~ The main interface."""
        head = dropsignal.web.html.header(static_path='/static/',
                                          scripts=['uri_variables', 'dynamics', 'main_dynamics',
                                                   'selector', 'plate', 'legend'],
                                          title='{} Dropsignal'.format(self.path),
                                          active='Dynamics',
                                          lib=['jquery', 'd3v4', 'bootstrap'],
                                          plugins=self.plugins,
                                          links=self.links)
        with open(os.path.join(APP_PATH, 'static', 'html', 'dynamics.html'), 'r') as file:
            content = file.read()
        return head + content + dropsignal.web.html.footer()

    @cherrypy.expose
    def explore_signal(self, **_):
        """Signal tab ~ Raw signal explorer"""
        head = dropsignal.web.html.header(static_path='/static/',
                                          scripts=['uri_variables', 'raw_explorer'],
                                          title="Signal",
                                          plugins=self.plugins,
                                          lib=['jquery', 'd3v4', 'bootstrap'],
                                          links=self.links,
                                          active="Signal")
        content = '''
        <div id="hud"></div>
        <main  class="container-fluid">
        <div class="row">
        <svg> </svg>
        </div>
        </main>
        '''
        return head + content + dropsignal.web.html.footer()

    @cherrypy.expose()
    def log(self):
        """Log tab ~ pretty print the pipeline.log"""
        head = dropsignal.web.html.header(static_path='/static/',
                                          scripts=[],
                                          title="Log",
                                          plugins=self.plugins,
                                          lib=['jquery', 'bootstrap'],
                                          links=self.links,
                                          active="Log")
        with open(os.path.join(self.path, 'analysis', 'pipeline_log.txt')) as file:
            txt = '</br>'.join(map(format_log, file))
        content = '<main  class="container-fluid">'
        if self.data_plugin.runs is not None:
            content += '<h1> Quality Control </h1> <h2> Run </h2>'
            cols = [x for x in ['peaks','good','complete_coalescence_information','tags']
                    if x in self.data_plugin.runs.columns]

            colnames = {'good':'Kept for analysis',
                        'peaks':'Number of peaks detected',
                        'tags':'Tags',
                        'complete_coalescence_information':'Complete coalescence information'}
            tbl_text =  self.data_plugin.runs.loc[:,cols].rename(columns=colnames).transpose().to_html(classes=['table','table-striped','table-overflow','table-condensed'])
            tbl_text = re.sub("True", '<span class="label label-success">Yes</span>', tbl_text)
            tbl_text = re.sub("False", '<span class="label label-danger">No</span>', tbl_text)
            tbl_text = re.sub("\[\]", '', tbl_text)
            content += '<div style="max-width:100%;overflow-x:scroll;">'+tbl_text+'</div>'

            if self.data_plugin.droplets is not None:
                content += '<h2> Droplets </h2>'
                drops = self.data_plugin.droplets.copy()
                mp = {int(x[0]):int(x[1])+1 for x in self._metadata['sorting_mapping_id_exp_to_id_run']}
                drops['Sorting position'] = [mp[x] if x in mp else 'NONE' for x in drops.droplet]
                drops['droplet'] += 1
                cols = [x for x in ['well', 'records', 'Sorting position', 'tags']
                        if x in drops.columns]
                colnames = {'records':'Number of entries', 'well':'Well', 'tags':'Tags'}
                tbl_text = drops.set_index('droplet').loc[:,cols].transpose().to_html(classes=['table','table-striped','table-overflow','table-condensed'])
                tbl_text = re.sub("NONE", '<span class="label label-danger">NONE</span>', tbl_text)
                tbl_text = re.sub("\[\]", '', tbl_text)
                content += '<div style="max-width:100%;overflow-x:scroll;">'+tbl_text+'</div>'
        content += '''
        <h1>Analysis log</h1>
        {}
        </main>
        '''.format(txt)
        return head + content + dropsignal.web.html.footer()

    @cherrypy.expose()
    def plugin(self, name):
        """Plugin tab ~ display the result of the plugin `name`"""
        header = dropsignal.web.html.header(static_path='/static/',
                                            scripts=[],
                                            title="Plugins",
                                            plugins=self.plugins,
                                            lib=['jquery', 'bootstrap'],
                                            links=self.links,
                                            active="Plugins")
        try:
            content = dropsignal.plugins.html_render(name, self.path, self._metadata)
        except Exception as ex:
            content = ('<div class="alert alert-danger" role="alert">'
                       '<p><strong>Error while running plugin</strong>: {}</p>'
                       '<p>{}</p>'
                       '</div>').format(name, ex)
        return header + content + dropsignal.web.html.footer()

    def teardown(self):
        """ Stop the plugins when closing the app """
        self.rawdata_plugin.stop()
        self.data_plugin.stop()

class RawExperiment(Experiment):
    """ Special case of experiment if no analysis was performed yet. """

    def __init__(self, path):
        self.path = path

        # No configuration, no metadata, no plugins.
        self.conf = {}
        self._metadata = None
        self.plugins = None

        # Channel prefix for cherrypy bus.
        self.channel_prefix = sha1((str(self.path)+'RAW').encode('utf-8')).hexdigest()

        # Start the plugin serving raw singal data.
        self.rawdata_plugin = RawDataPlugin(cherrypy.engine, path,
                                            self.channel_prefix)
        self.rawdata_plugin.subscribe() #subscribe to start and stop channels of the bus.
        self.rawdata_plugin.start()

        # Set the links at the top of each page
        self.links = [('Home', '/'),
                      ('Close', '/stop_exp?path={}'.format(self.path))]

        # The landing page is explore_signal
        self.index = self.explore_signal

    def teardown(self):
        """ Stop the plugin when closing the app """
        self.rawdata_plugin.stop()

"""webgui.py ~ web application serving the data
This file is part of the dropsignal package.
Copyright 2016-2019 Guilhem Doulcier, Licence GNU AGPL3+
"""

from dropsignal.web import main
if __name__ == '__main__':
    main()
